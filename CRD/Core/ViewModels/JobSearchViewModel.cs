﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

namespace CRD.Core.ViewModels
{
    public class JobSearchViewModel
    {
        public JobSearchViewModel()
        {
            IncludeEmployers = true;
            IncludeRecruitmentAgencies = true;
            SearchTerm = "";
            MaxElementsToRetrieve = 50;

            MaxElementValues = new List<SelectListItem>
            {
                new SelectListItem() {Text = "10", Value = "10"},
                new SelectListItem() {Text = "20", Value = "20"},
                new SelectListItem() {Text = "30", Value = "30"},
                new SelectListItem() {Text = "50", Value = "50"}
            };
        }

        [DisplayName("Include Employers")]
        public bool IncludeEmployers { get; set; }

        [DisplayName("Include Recruiters")]
        public bool IncludeRecruitmentAgencies { get; set; }

        [DisplayName("Search:")]
        public string SearchTerm { get; set; }

        public IEnumerable<JobPositionViewModel> JobPositions { get; set; }

        [DisplayName("Elements:")]
        public int MaxElementsToRetrieve { get; set; }

        public IEnumerable<SelectListItem> MaxElementValues { get; set; }
    }
}