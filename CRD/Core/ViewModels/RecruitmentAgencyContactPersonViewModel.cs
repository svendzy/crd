﻿using System.ComponentModel.DataAnnotations;

namespace CRD.Core.ViewModels
{
    public class RecruitmentAgencyContactPersonViewModel
    {
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Name is a required field")]
        [StringLength(50, ErrorMessage = "Name has a maximum length of 50 characters")]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Title is a required field")]
        [StringLength(50, ErrorMessage = "Title has a maximum length of 50 characters")]
        public string Title { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Email is a required field")]
        [EmailAddress(ErrorMessage = "Invalid email address!")]
        public string Email { get; set; }

        public string Phone { get; set; }
    }
}