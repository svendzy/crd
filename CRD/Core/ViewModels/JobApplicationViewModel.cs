﻿using System.ComponentModel.DataAnnotations;

namespace CRD.Core.ViewModels
{
    public class JobApplicationViewModel
    {
        public int Id { get; set; }

        public JobPositionViewModel JobPosition { get; set; } 

        [Required(AllowEmptyStrings = false, ErrorMessage = "Text is a required field")]
        [StringLength(1000, ErrorMessage = "Text must not exceed 1000 characters")]
        public string Text { get; set; }
    }
}