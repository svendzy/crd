﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using CRD.Core.Attributes;

namespace CRD.Core.ViewModels
{
    public abstract class JobPositionViewModel
    {
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Job Title is a required field")]
        [DisplayName("Job Title")]
        [StringLength(100, ErrorMessage="Job Title cannot exceed 100 characters")]
        public string JobTitle { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Job Description is a required field")]
        [DisplayName("Job Description")]
        [StringLength(1000, ErrorMessage = "Job Title cannot exceed 100 characters")]
        public string JobDescription { get; set; }

        [DisplayName("Posted Date")]
        public string PostedDate { get; set; }

        [Required]
        [DisplayName("Expiry Date")]
        [FutureDate(ErrorMessage="Invalid date format or non-future date specified")]
        public string ExpiryDate { get; set; }
    }
}