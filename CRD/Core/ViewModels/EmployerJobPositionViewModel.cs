﻿using CRD.Core.Models;
using System.Collections.Generic;

namespace CRD.Core.ViewModels
{
    public class EmployerJobPositionViewModel : JobPositionViewModel
    {
        public EmployerJobPositionViewModel()
        {
        }

        public EmployerJobPositionViewModel(EmployerJobPosition employerJobPosition)
        {
            Id = employerJobPosition.Id;
            CompanyName = employerJobPosition.Employer.CompanyName;
            JobTitle = employerJobPosition.JobTitle;
            JobDescription = employerJobPosition.JobDescription;
            PostedDate = employerJobPosition.PostedDate.ToString("dd.MM.yyyy");
            ExpiryDate = employerJobPosition.ExpiryDate.ToString("dd.MM.yyyy");
        }

        public string CompanyName { get; set; }
        public IEnumerable<EmployerContactPersonViewModel> Contacts { get; set; }
    }
}