﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CRD.Core.ViewModels
{
    public class RecruitmentAgencyAccountViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Company Name is a required field")]
        [StringLength(50, ErrorMessage = "Company Name must not exceed 50 characters")]
        [DisplayName("Company name")]
        public string CompanyName { get; set; }


        [Required(ErrorMessage = "Email is a required field")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [DisplayName("Email")]
        public string Email { get; set; }


        [Required(ErrorMessage = "Password is a required field")]
        [StringLength(20, ErrorMessage = "Password must not exceed 20 characters and digits")]
        [DisplayName("Password")]
        public string Password { get; set; }
    }
}