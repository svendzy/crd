using CRD.Core.Models;
using System.Collections.Generic;

namespace CRD.Core.ViewModels
{
    public class RecruitmentAgencyJobPositionViewModel : JobPositionViewModel
    {
        public RecruitmentAgencyJobPositionViewModel()
        {
        }

        public RecruitmentAgencyJobPositionViewModel(RecruitmentAgencyJobPosition recruitmentAgencyJobPosition)
        {
            Id = recruitmentAgencyJobPosition.Id;
            CompanyName = recruitmentAgencyJobPosition.RecruitmentAgency.CompanyName;
            JobTitle = recruitmentAgencyJobPosition.JobTitle;
            JobDescription = recruitmentAgencyJobPosition.JobDescription;
            PostedDate = recruitmentAgencyJobPosition.PostedDate.ToString("dd.MM.yyyy");
            ExpiryDate = recruitmentAgencyJobPosition.ExpiryDate.ToString("dd.MM.yyyy");
        }

        public string CompanyName { get; set; }
        public IEnumerable<RecruitmentAgencyContactPersonViewModel> Contacts { get; set; }
    }
}