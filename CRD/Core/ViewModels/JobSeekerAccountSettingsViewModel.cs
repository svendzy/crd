﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CRD.Core.ViewModels
{
    public class JobSeekerAccountSettingsViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "First name is a required field")]
        [StringLength(50, ErrorMessage = "First name must not exceed 50 characters")]
        [DisplayName("First name")]
        public string Firstname { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Last name is a required field")]
        [StringLength(50, ErrorMessage = "Last name must not exceed 50 characters")]
        [DisplayName("Last name")]
        public string Lastname { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Address is a required field")]
        [StringLength(100, ErrorMessage = "Address must not exceed 50 characters")]
        [DisplayName("Address")]
        public string Address { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Zip Code is a required field")]
        [StringLength(4, ErrorMessage = "Zip Code must not exceed 4 characters")]
        [DisplayName("Zip Code")]
        public string Zipcode { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "City is a required field")]
        [StringLength(50, ErrorMessage = "City must not exceed 50 characters")]
        [DisplayName("City")]
        public string City { get; set; }

        [DisplayName("Home Page")]
        public string Homepage { get; set; }      
    }
}