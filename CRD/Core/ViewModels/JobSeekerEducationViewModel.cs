﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CRD.Core.ViewModels
{
    public class JobSeekerEducationViewModel
    {
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "From Date is a required field")]
        [DisplayName("From")]
        [RegularExpression(@"^[0123]\d{1}\.[0123]\d{1}\.[12]\d{3}$", ErrorMessage = "Wrong Date format!")]
        public string FromDate { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "To Date is a required field")]
        [DisplayName("To")]
        [RegularExpression(@"^[0123]\d{1}\.[0123]\d{1}\.[12]\d{3}$", ErrorMessage = "Wrong Date format!")]
        public string ToDate { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Place is a required field")]
        [DisplayName("Place")]
        [StringLength(100, ErrorMessage = "Place cannot exceed 100 characters")]
        public string Place { get; set; }

        [Required(AllowEmptyStrings = true, ErrorMessage = "Description is a required field")]
        [DisplayName("Description")]
        [StringLength(200, ErrorMessage = "Description cannot exceed 200 characters")]
        public string Description { get; set; }
    }
}