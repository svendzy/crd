﻿using CRD.Core.Models;
using System.Collections.Generic;

namespace CRD.Core.Repositories
{
    public interface IEmployerAccountRepository
    {
        IEnumerable<EmployerAccount> GetAll();
        void Add(EmployerAccount employerAccount);
        EmployerAccount FindByAppUser(string userId);
        EmployerAccount Get(int id);
    }
}