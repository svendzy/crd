using System.Collections.Generic;
using CRD.Core.Models;

namespace CRD.Core.Repositories
{
    public interface IEmployerContactPersonRepository
    {
        IEnumerable<EmployerContactPerson> GetAll(int employerId);
        void Add(EmployerContactPerson contactPerson);
        EmployerContactPerson Find(int id);
        void Remove(EmployerContactPerson contactPerson);
    }
}