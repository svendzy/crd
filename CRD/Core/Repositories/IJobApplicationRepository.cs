using CRD.Core.Models;
using System.Collections.Generic;

namespace CRD.Core.Repositories
{
    public interface IJobApplicationRepository
    {
        IEnumerable<JobApplication> GetAll(int jobSeekerId);
        IEnumerable<JobApplication> GetLast10(int jobSeekerId);
        IList<int> GetAppliedJobPositionsIds(int jobSeekerId);
        void Add(JobApplication jobApplication);
        JobApplication Find(int id);
        void Remove(JobApplication jobApplication);
    }
}