﻿using CRD.Core.Models;
using System.Collections.Generic;

namespace CRD.Core.Repositories
{
    public interface IRecruitmentAgencyContactPersonRepository
    {
        IEnumerable<RecruitmentAgencyContactPerson> GetAll(int recruiterId);
        void Add(RecruitmentAgencyContactPerson contactPerson);
        RecruitmentAgencyContactPerson Find(int id);
        void Remove(RecruitmentAgencyContactPerson contactPerson);
    }
}