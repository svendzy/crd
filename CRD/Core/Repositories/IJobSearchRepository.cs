using System.Collections.Generic;
using CRD.Core.Models;

namespace CRD.Core.Repositories
{
    public interface IJobSearchRepository
    {
        IEnumerable<RecruitmentAgencyJobPosition> SearchByRecruiters(int maxElementsToRetieve, string searchTerm);
        IEnumerable<EmployerJobPosition> SearchByEmployers(int maxElementsToRetieve, string searchTerm);
    }
}