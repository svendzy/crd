﻿using CRD.Core.Models;
using System.Collections.Generic;

namespace CRD.Core.Repositories
{
    public interface IJobSeekerExperienceRepository
    {
        IEnumerable<JobSeekerExperience> GetAll(int jobSeekerId);
        void Add(JobSeekerExperience jobSeekerExperience);
        JobSeekerExperience Find(int id);
        void Remove(JobSeekerExperience jobSeekerExperience);
    }
}