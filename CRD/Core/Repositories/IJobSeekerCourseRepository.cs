using System.Collections.Generic;
using CRD.Core.Models;

namespace CRD.Core.Repositories
{
    public interface IJobSeekerCourseRepository
    {
        IEnumerable<JobSeekerCourse> GetAll(int jobSeekerId);
        void Add(JobSeekerCourse jobSeekerCourse);
        JobSeekerCourse Find(int id);
        void Remove(JobSeekerCourse jobSeekerCourse);
    }
}