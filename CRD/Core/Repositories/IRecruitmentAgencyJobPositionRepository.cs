using CRD.Core.Models;
using System.Collections.Generic;

namespace CRD.Core.Repositories
{
    public interface IRecruitmentAgencyJobPositionRepository
    {
        IEnumerable<RecruitmentAgencyJobPosition> GetAll(int recruitmentAgencyId);
        IEnumerable<RecruitmentAgencyJobPosition> GetLast10(int recruitmentAgencyId);
        void Add(RecruitmentAgencyJobPosition recruitmentAgencyJobPosition);
        RecruitmentAgencyJobPosition Find(int id);
        void Remove(RecruitmentAgencyJobPosition recruitmentAgencyJobPosition);
    }
}