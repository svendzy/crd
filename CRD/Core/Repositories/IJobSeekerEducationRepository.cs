﻿using System.Collections.Generic;
using CRD.Core.Models;

namespace CRD.Core.Repositories
{
    public interface IJobSeekerEducationRepository
    {
        IEnumerable<JobSeekerEducation> GetAll(int jobSeekerId);
        void Add(JobSeekerEducation jobSeekerEducation);
        JobSeekerEducation Find(int id);
        void Remove(JobSeekerEducation jobSeekerEducation);
    }
}