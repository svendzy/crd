﻿using CRD.Core.Models;
using System.Threading.Tasks;

namespace CRD.Core.Repositories
{
    public interface IJobSeekerAccountRepository
    {
        void Add(JobSeekerAccount jobSeekerAccount);
        Task<JobSeekerAccount> FindAsync(int jobSeekerId);
        JobSeekerAccount FindByAppUser(string userId);
    }
}