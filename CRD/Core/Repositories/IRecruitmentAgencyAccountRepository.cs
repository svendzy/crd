﻿using CRD.Core.Models;
using System.Collections.Generic;

namespace CRD.Core.Repositories
{
    public interface IRecruitmentAgencyAccountRepository
    {
        void Add(RecruitmentAgencyAccount recruitmentAgencyAccount);
        IEnumerable<RecruitmentAgencyAccount> GetAll();
        RecruitmentAgencyAccount FindByAppUser(string userId);
        RecruitmentAgencyAccount Get(int id);
    }
}