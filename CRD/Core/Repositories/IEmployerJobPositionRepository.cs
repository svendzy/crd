using CRD.Core.Models;
using System.Collections.Generic;

namespace CRD.Core.Repositories
{
    public interface IEmployerJobPositionRepository
    {
        IEnumerable<EmployerJobPosition> GetAll(int employerId);
        IEnumerable<EmployerJobPosition> GetLast10(int employerId);
        void Add(EmployerJobPosition employerJobPosition);
        EmployerJobPosition Find(int id);
        void Remove(EmployerJobPosition employerJobPosition);
    }
}