﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRD.Core.Models
{
    public class EmployerJobPositionContactPerson
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int EmployerContactPersonId { get; set; }

        [ForeignKey("EmployerContactPersonId")]
        public EmployerContactPerson EmployerContactPerson { get; set; }

        [Required]
        public int EmployerJobPositionId { get; set; }

        [ForeignKey("EmployerJobPositionId")]
        public EmployerJobPosition EmployerJobPosition { get; set; }
    }
}