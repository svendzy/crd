﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRD.Core.Models
{
    public class JobApplication
    {
        [Key]
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        [MaxLength(1000)]
        public string Text { get; set; }

        [Required]
        public int JobPositionId { get; set; }

        [ForeignKey("JobPositionId")]
        public JobPosition JobPosition { get; set; }

        [Required]
        public int JobSeekerId { get; set; }

        [ForeignKey("JobSeekerId")]
        public JobSeekerAccount JobSeeker { get; set; }
    }
}