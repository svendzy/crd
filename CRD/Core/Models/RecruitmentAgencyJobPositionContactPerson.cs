﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRD.Core.Models
{
    public class RecruitmentAgencyJobPositionContactPerson
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int RecruitmentAgencyContactPersonId { get; set; }

        [ForeignKey("RecruitmentAgencyContactPersonId")]
        public RecruitmentAgencyContactPerson RecruitmentAgencyContactPerson { get; set; }

        [Required]
        public int RecruitmentAgencyJobPositionId { get; set; }

        [ForeignKey("RecruitmentAgencyJobPositionId")]
        public RecruitmentAgencyJobPosition RecruitmentAgencyJobPosition { get; set; }
    }
}