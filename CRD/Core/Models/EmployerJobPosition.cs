﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRD.Core.Models
{
    [Table("EmployerJobPosition")]
    public class EmployerJobPosition : JobPosition
    {
        public EmployerJobPosition()
        {
            Contacts = new Collection<EmployerJobPositionContactPerson>();
        }

        [Required]
        public int EmployerId { get; set; }

        [ForeignKey("EmployerId")]
        public EmployerAccount Employer { get; set; }

        public ICollection<EmployerJobPositionContactPerson> Contacts { get; set; }
    }
}