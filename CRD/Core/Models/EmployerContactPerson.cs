﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRD.Core.Models
{
    public class EmployerContactPerson
    {
        [Key]
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false)]
        [MaxLength(50)]
        public string Title { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Email { get; set; }

        public string Phone { get; set; }

        [Required]
        public int EmployerId { get; set; }

        [ForeignKey("EmployerId")]
        public EmployerAccount Employer { get; set; }
    }
}