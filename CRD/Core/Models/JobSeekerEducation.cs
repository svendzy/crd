﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRD.Core.Models
{
    public class JobSeekerEducation
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public DateTime FromDate { get; set; }

        [Required]
        public DateTime ToDate { get; set; }

        [Required(AllowEmptyStrings = false)]
        [MaxLength(100)]
        public string Place { get; set; }

        [Required(AllowEmptyStrings = true)]
        [MaxLength(200)]
        public string Description { get; set; }

        [Required]
        public int JobSeekerId { get; set; }

        [ForeignKey("JobSeekerId")]
        public JobSeekerAccount JobSeeker { get; set; }
    }
}