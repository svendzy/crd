﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRD.Core.Models
{
    public class RecruitmentAgencyAccount
    {
        [Key]
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        [MaxLength(50)]
        [Index("IX_Ra_CompanyName", 1, IsUnique = true)]
        public string CompanyName { get; set; }

        [Required]
        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public ApplicationUser ApplicationUser { get; set; }

        public virtual ICollection<RecruitmentAgencyJobPosition> Positions { get; set; }

        public virtual ICollection<RecruitmentAgencyContactPerson> Contacts { get; set; }
    }
}