﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CRD.Core.Models
{
    public abstract class JobPosition
    {
        [Key]
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        [MaxLength(100)]
        public string JobTitle { get; set; }

        [Required(AllowEmptyStrings = false)]
        [MaxLength(1000)]
        public string JobDescription { get; set; }

        [Required]
        public DateTime PostedDate { get; set; }

        [Required]
        public DateTime ExpiryDate { get; set; }

        public virtual ICollection<JobApplication> Applications { get; set; }
    }
}