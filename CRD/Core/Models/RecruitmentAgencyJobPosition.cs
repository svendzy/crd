﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRD.Core.Models
{
    [Table("RecruitmentAgencyJobPosition")]
    public class RecruitmentAgencyJobPosition : JobPosition
    {
        public ICollection<RecruitmentAgencyJobPositionContactPerson> Contacts { get; set; }

        [Required]
        public int RecruitmentAgencyId { get; set; }

        [ForeignKey("RecruitmentAgencyId")]
        public RecruitmentAgencyAccount RecruitmentAgency { get; set; }
    }
}