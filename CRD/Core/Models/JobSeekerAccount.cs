﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRD.Core.Models
{
    public class JobSeekerAccount
    {
        [Key]
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        [MaxLength(50)]
        public string Firstname { get; set; }

        [Required(AllowEmptyStrings = false)]
        [MaxLength(50)]
        public string Lastname { get; set; }

        [Required(AllowEmptyStrings = false)]
        [MaxLength(100)]
        public string Address { get; set; }

        [Required(AllowEmptyStrings = false)]
        [MaxLength(4)]
        public string Zipcode { get; set; }

        [Required(AllowEmptyStrings = false)]
        [MaxLength(50)]
        public string City { get; set; }

        public string Homepage { get; set; }

        [Required]
        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public ApplicationUser ApplicationUser { get; set; }

        public virtual ICollection<JobApplication> Applications { get; set; }

        public virtual ICollection<JobSeekerEducation> Educations { get; set; }

        public virtual ICollection<JobSeekerExperience> Experiences { get; set; }

        public virtual ICollection<JobSeekerCourse> Courses { get; set; }
    }
}