﻿using System;
using System.Web.Mvc;

namespace CRD.Core.Attributes
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }

            if ((!filterContext.HttpContext.User.Identity.IsAuthenticated) && 
                (!filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true)))
            {
                var loginUrl = "/login";
                filterContext.Result = new RedirectResult(loginUrl);
            }
        }
    }
}