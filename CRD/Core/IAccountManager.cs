using CRD.Core.Models;
using System.Threading.Tasks;

namespace CRD.Core
{
    public interface IAccountManager
    {
        Task<JobSeekerAccount> CreateJobSeekerAccount(string email, string pwd, JobSeekerAccount account);
        Task<EmployerAccount> CreateEmployerAccount(string email, string pwd, EmployerAccount account);
        Task<RecruitmentAgencyAccount> CreateRecruitmentAgencyAccount(string email, string pwd, RecruitmentAgencyAccount account);
    }
}