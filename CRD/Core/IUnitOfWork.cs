using CRD.Core.Repositories;

namespace CRD.Core
{
    public interface IUnitOfWork
    {
        IRecruitmentAgencyJobPositionRepository RecruitmentAgencyJobPositions { get; }
        IRecruitmentAgencyContactPersonRepository RecruitmentAgencyContactPersons { get; }
        IRecruitmentAgencyAccountRepository RecruitmentAgencies { get; }
        IJobSeekerExperienceRepository JobSeekerExperiences { get; }
        IJobSeekerEducationRepository JobSeekerEducations { get; }
        IJobSeekerCourseRepository JobSeekerCourses { get; }
        IJobSeekerAccountRepository JobSeekers { get; }
        IJobSearchRepository JobSearches { get; }
        IJobApplicationRepository JobApplications { get; }
        IEmployerJobPositionRepository EmployerJobPositions { get; }
        IEmployerContactPersonRepository EmployerContactPersons { get; }
        IEmployerAccountRepository Employers { get; }
        void Complete();
    }
}