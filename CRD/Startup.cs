﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CRD.Startup))]
namespace CRD
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
