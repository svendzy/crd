﻿using System.Data.Entity;
using CRD.Core.Models;

namespace CRD.Persistence
{
    public interface IApplicationDbContext
    {
        DbSet<JobSeekerAccount> JobSeekers { get; set; }
        DbSet<JobSeekerEducation> JobSeekerEducation { get; set; }
        DbSet<JobSeekerExperience> JobSeekerExperience { get; set; }
        DbSet<JobSeekerCourse> JobSeekerCourses { get; set; }
        DbSet<JobApplication> JobApplications { get; set; }
        DbSet<EmployerJobPosition> EmployerJobPositions { get; set; }
        DbSet<JobPosition> JobPositions { get; set; }
        DbSet<EmployerAccount> Employers { get; set; }
        DbSet<EmployerContactPerson> EmployerContactPersons { get; set; }
        DbSet<RecruitmentAgencyAccount> RecruitmentAgencies { get; set; }
        DbSet<RecruitmentAgencyJobPosition> RecruitmentAgencyJobPositions { get; set; }
        DbSet<RecruitmentAgencyContactPerson> RecruitmentAgencyContactPersons { get; set; }
    }
}