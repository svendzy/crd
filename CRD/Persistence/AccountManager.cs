﻿using CRD.Core;
using CRD.Core.Models;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using System.Web;

namespace CRD.Persistence
{
    public class AccountManager : IAccountManager
    {
        public async Task<JobSeekerAccount> CreateJobSeekerAccount(string email, string pwd, JobSeekerAccount account)
        {
            var userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();

            var dataContext = HttpContext.Current.GetOwinContext().Get<ApplicationDbContext>();

            using (var trans = dataContext.Database.BeginTransaction())
            {
                try
                {
                    var user = new ApplicationUser { UserName = email, Email = email };
                    var result = await userManager.CreateAsync(user, pwd);
                    if (!result.Succeeded) return null;
                    var result2 = await userManager.AddToRoleAsync(user.Id, "JobSeeker");
                    if (!result2.Succeeded) return null;

                    var jobSeekerAccount = new JobSeekerAccount
                    {
                        Firstname = account.Firstname,
                        Lastname = account.Lastname,
                        Address = account.Address,
                        Zipcode = account.Zipcode,
                        City = account.City,
                        Homepage = account.Homepage ?? "",
                        UserId = user.Id,
                        ApplicationUser = user
                    };

                    dataContext.JobSeekers.Add(jobSeekerAccount);
                    await dataContext.SaveChangesAsync();

                    trans.Commit();

                    return jobSeekerAccount;
                }
                catch
                {
                    trans.Rollback();
                    throw;
                }
            }
        }

        public async Task<EmployerAccount> CreateEmployerAccount(string email, string pwd, EmployerAccount account)
        {
            var userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();

            var dataContext = HttpContext.Current.GetOwinContext().Get<ApplicationDbContext>();

            using (var trans = dataContext.Database.BeginTransaction())
            {
                try
                {
                    var user = new ApplicationUser { UserName = email, Email = email };
                    var result = await userManager.CreateAsync(user, pwd);
                    if (!result.Succeeded) return null;
                    var result2 = await userManager.AddToRoleAsync(user.Id, "Employer");
                    if (!result2.Succeeded) return null;

                    var employerAccount = new EmployerAccount
                    {
                        CompanyName = account.CompanyName,
                        UserId = user.Id,
                        ApplicationUser = user
                    };

                    dataContext.Employers.Add(employerAccount);
                    await dataContext.SaveChangesAsync();

                    trans.Commit();

                    return employerAccount;
                }
                catch
                {
                    trans.Rollback();
                    throw;
                }
            }
        }

        public async Task<RecruitmentAgencyAccount> CreateRecruitmentAgencyAccount(string email, string pwd, RecruitmentAgencyAccount account)
        {
            var userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();

            var dataContext = HttpContext.Current.GetOwinContext().Get<ApplicationDbContext>();

            using (var trans = dataContext.Database.BeginTransaction())
            {
                try
                {
                    var user = new ApplicationUser { UserName = email, Email = email };
                    var result = await userManager.CreateAsync(user, pwd);
                    if (!result.Succeeded) return null;
                    var result2 = await userManager.AddToRoleAsync(user.Id, "RecruitmentAgency");
                    if (!result2.Succeeded) return null;

                    var recruitmentAgencyAccount = new RecruitmentAgencyAccount
                    {
                        CompanyName = account.CompanyName,
                        UserId = user.Id,
                        ApplicationUser = user
                    };

                    dataContext.RecruitmentAgencies.Add(recruitmentAgencyAccount);
                    await dataContext.SaveChangesAsync();

                    trans.Commit();

                    return recruitmentAgencyAccount;
                }
                catch
                {
                    trans.Rollback();
                    throw;
                }
            }
        }
    }
}