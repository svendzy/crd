using System.Data.Entity.Migrations;

namespace CRD.Migrations
{
    public partial class Updatetables_3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CompanyLogoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Filename = c.String(nullable: false),
                        Data = c.Binary(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.EmployerAccounts", "CompanyLogoId", c => c.Int(nullable: false));
            AddColumn("dbo.RecruitmentAgencyAccounts", "CompanyLogoId", c => c.Int(nullable: false));
            CreateIndex("dbo.EmployerAccounts", "CompanyLogoId");
            CreateIndex("dbo.RecruitmentAgencyAccounts", "CompanyLogoId");
            AddForeignKey("dbo.EmployerAccounts", "CompanyLogoId", "dbo.CompanyLogoes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.RecruitmentAgencyAccounts", "CompanyLogoId", "dbo.CompanyLogoes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RecruitmentAgencyAccounts", "CompanyLogoId", "dbo.CompanyLogoes");
            DropForeignKey("dbo.EmployerAccounts", "CompanyLogoId", "dbo.CompanyLogoes");
            DropIndex("dbo.RecruitmentAgencyAccounts", new[] { "CompanyLogoId" });
            DropIndex("dbo.EmployerAccounts", new[] { "CompanyLogoId" });
            DropColumn("dbo.RecruitmentAgencyAccounts", "CompanyLogoId");
            DropColumn("dbo.EmployerAccounts", "CompanyLogoId");
            DropTable("dbo.CompanyLogoes");
        }
    }
}
