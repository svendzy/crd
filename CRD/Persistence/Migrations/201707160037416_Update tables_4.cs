using System.Data.Entity.Migrations;

namespace CRD.Migrations
{
    public partial class Updatetables_4 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.EmployerAccounts", "CompanyLogoId", "dbo.CompanyLogoes");
            DropForeignKey("dbo.RecruitmentAgencyAccounts", "CompanyLogoId", "dbo.CompanyLogoes");
            DropIndex("dbo.EmployerAccounts", new[] { "CompanyLogoId" });
            DropIndex("dbo.RecruitmentAgencyAccounts", new[] { "CompanyLogoId" });
            DropColumn("dbo.EmployerAccounts", "CompanyLogoId");
            DropColumn("dbo.RecruitmentAgencyAccounts", "CompanyLogoId");
            DropTable("dbo.CompanyLogoes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.CompanyLogoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Filename = c.String(nullable: false),
                        Data = c.Binary(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.RecruitmentAgencyAccounts", "CompanyLogoId", c => c.Int(nullable: false));
            AddColumn("dbo.EmployerAccounts", "CompanyLogoId", c => c.Int(nullable: false));
            CreateIndex("dbo.RecruitmentAgencyAccounts", "CompanyLogoId");
            CreateIndex("dbo.EmployerAccounts", "CompanyLogoId");
            AddForeignKey("dbo.RecruitmentAgencyAccounts", "CompanyLogoId", "dbo.CompanyLogoes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.EmployerAccounts", "CompanyLogoId", "dbo.CompanyLogoes", "Id", cascadeDelete: true);
        }
    }
}
