namespace CRD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Updatetables_10 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.JobSeekerAccounts", "Homepage", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.JobSeekerAccounts", "Homepage", c => c.String(nullable: false));
        }
    }
}
