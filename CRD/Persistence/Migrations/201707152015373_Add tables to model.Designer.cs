// <auto-generated />

using System.CodeDom.Compiler;
using System.Data.Entity.Migrations.Infrastructure;
using System.Resources;

namespace CRD.Migrations
{
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Addtablestomodel : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Addtablestomodel));
        
        string IMigrationMetadata.Id
        {
            get { return "201707152015373_Add tables to model"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
