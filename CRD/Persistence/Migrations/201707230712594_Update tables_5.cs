using System.Data.Entity.Migrations;

namespace CRD.Migrations
{
    public partial class Updatetables_5 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.JobSeekerDocuments", "JobSeekerId", "dbo.JobSeekerAccounts");
            DropIndex("dbo.JobSeekerDocuments", new[] { "JobSeekerId" });
            DropTable("dbo.JobSeekerDocuments");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.JobSeekerDocuments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Filename = c.String(nullable: false),
                        Data = c.Binary(nullable: false),
                        JobSeekerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.JobSeekerDocuments", "JobSeekerId");
            AddForeignKey("dbo.JobSeekerDocuments", "JobSeekerId", "dbo.JobSeekerAccounts", "Id", cascadeDelete: true);
        }
    }
}
