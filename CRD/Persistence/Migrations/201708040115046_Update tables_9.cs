using System.Data.Entity.Migrations;

namespace CRD.Migrations
{
    public partial class Updatetables_9 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.RecruitmentAgencyContactPersons", "Name", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.RecruitmentAgencyContactPersons", "Title", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.RecruitmentAgencyContactPersons", "Email", c => c.String(nullable: false));
            AlterColumn("dbo.EmployerContactPersons", "Name", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.EmployerContactPersons", "Title", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.EmployerContactPersons", "Email", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.EmployerContactPersons", "Email", c => c.String());
            AlterColumn("dbo.EmployerContactPersons", "Title", c => c.String());
            AlterColumn("dbo.EmployerContactPersons", "Name", c => c.String());
            AlterColumn("dbo.RecruitmentAgencyContactPersons", "Email", c => c.String());
            AlterColumn("dbo.RecruitmentAgencyContactPersons", "Title", c => c.String());
            AlterColumn("dbo.RecruitmentAgencyContactPersons", "Name", c => c.String());
        }
    }
}
