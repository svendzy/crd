using System.Data.Entity.Migrations;

namespace CRD.Migrations
{
    public partial class Updatetables_6 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.JobSeekerAccounts", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.JobSeekerAccounts", new[] { "UserId" });
            AlterColumn("dbo.JobSeekerAccounts", "UserId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.JobSeekerAccounts", "UserId");
            AddForeignKey("dbo.JobSeekerAccounts", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.JobSeekerAccounts", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.JobSeekerAccounts", new[] { "UserId" });
            AlterColumn("dbo.JobSeekerAccounts", "UserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.JobSeekerAccounts", "UserId");
            AddForeignKey("dbo.JobSeekerAccounts", "UserId", "dbo.AspNetUsers", "Id");
        }
    }
}
