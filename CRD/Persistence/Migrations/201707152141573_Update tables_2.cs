using System.Data.Entity.Migrations;

namespace CRD.Migrations
{
    public partial class Updatetables_2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.EmployerAccounts", "CompanyLogo");
            DropColumn("dbo.RecruitmentAgencyAccounts", "CompanyLogo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.RecruitmentAgencyAccounts", "CompanyLogo", c => c.Binary());
            AddColumn("dbo.EmployerAccounts", "CompanyLogo", c => c.Binary());
        }
    }
}
