using System.Data.Entity.Migrations;

namespace CRD.Migrations
{
    public partial class Addtablestomodel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.JobSeekerDocuments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Filename = c.String(nullable: false),
                        Data = c.Binary(nullable: false),
                        JobSeekerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.JobSeekerAccounts", t => t.JobSeekerId, cascadeDelete: true)
                .Index(t => t.JobSeekerId);
            
            CreateTable(
                "dbo.JobSeekerEducations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FromDate = c.DateTime(nullable: false),
                        ToDate = c.DateTime(nullable: false),
                        Place = c.String(nullable: false, maxLength: 100),
                        Description = c.String(nullable: false, maxLength: 200),
                        JobSeekerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.JobSeekerAccounts", t => t.JobSeekerId, cascadeDelete: true)
                .Index(t => t.JobSeekerId);
            
            CreateTable(
                "dbo.JobSeekerExperiences",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FromDate = c.DateTime(nullable: false),
                        ToDate = c.DateTime(nullable: false),
                        Place = c.String(nullable: false, maxLength: 100),
                        Description = c.String(nullable: false, maxLength: 200),
                        JobSeekerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.JobSeekerAccounts", t => t.JobSeekerId, cascadeDelete: true)
                .Index(t => t.JobSeekerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.JobSeekerExperiences", "JobSeekerId", "dbo.JobSeekerAccounts");
            DropForeignKey("dbo.JobSeekerEducations", "JobSeekerId", "dbo.JobSeekerAccounts");
            DropForeignKey("dbo.JobSeekerDocuments", "JobSeekerId", "dbo.JobSeekerAccounts");
            DropIndex("dbo.JobSeekerExperiences", new[] { "JobSeekerId" });
            DropIndex("dbo.JobSeekerEducations", new[] { "JobSeekerId" });
            DropIndex("dbo.JobSeekerDocuments", new[] { "JobSeekerId" });
            DropTable("dbo.JobSeekerExperiences");
            DropTable("dbo.JobSeekerEducations");
            DropTable("dbo.JobSeekerDocuments");
        }
    }
}
