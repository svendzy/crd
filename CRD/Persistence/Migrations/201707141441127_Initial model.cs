using System.Data.Entity.Migrations;

namespace CRD.Migrations
{
    public partial class Initialmodel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EmployerAccounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompanyName = c.String(nullable: false, maxLength: 50),
                        CompanyLogo = c.Binary(),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.EmployerContactPersons",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Title = c.String(),
                        Email = c.String(),
                        Phone = c.String(),
                        EmployerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EmployerAccounts", t => t.EmployerId, cascadeDelete: true)
                .Index(t => t.EmployerId);
            
            CreateTable(
                "dbo.JobPositions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        JobTitle = c.String(nullable: false, maxLength: 100),
                        JobDescription = c.String(nullable: false, maxLength: 1000),
                        PostedDate = c.DateTime(nullable: false),
                        ExpiryDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EmployerJobPositionContactPersons",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployerContactPersonId = c.Int(nullable: false),
                        EmployerJobPositionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EmployerContactPersons", t => t.EmployerContactPersonId, cascadeDelete: true)
                .ForeignKey("dbo.EmployerJobPosition", t => t.EmployerJobPositionId)
                .Index(t => t.EmployerContactPersonId)
                .Index(t => t.EmployerJobPositionId);
            
            CreateTable(
                "dbo.JobApplications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(nullable: false, maxLength: 1000),
                        JobPositionId = c.Int(nullable: false),
                        JobSeekerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.JobPositions", t => t.JobPositionId, cascadeDelete: true)
                .ForeignKey("dbo.JobSeekerAccounts", t => t.JobSeekerId, cascadeDelete: true)
                .Index(t => t.JobPositionId)
                .Index(t => t.JobSeekerId);
            
            CreateTable(
                "dbo.RecruitmentAgencyJobPositionContactPersons",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RecruitmentAgencyContactPersonId = c.Int(nullable: false),
                        RecruitmentAgencyJobPositionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RecruitmentAgencyContactPersons", t => t.RecruitmentAgencyContactPersonId, cascadeDelete: true)
                .ForeignKey("dbo.RecruitmentAgencyJobPosition", t => t.RecruitmentAgencyJobPositionId)
                .Index(t => t.RecruitmentAgencyContactPersonId)
                .Index(t => t.RecruitmentAgencyJobPositionId);
            
            CreateTable(
                "dbo.RecruitmentAgencyContactPersons",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Title = c.String(),
                        Email = c.String(),
                        Phone = c.String(),
                        RecruitmentAgencyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RecruitmentAgencyAccounts", t => t.RecruitmentAgencyId, cascadeDelete: true)
                .Index(t => t.RecruitmentAgencyId);
            
            CreateTable(
                "dbo.RecruitmentAgencyAccounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompanyName = c.String(nullable: false, maxLength: 50),
                        CompanyLogo = c.Binary(),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.JobSeekerAccounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Firstname = c.String(nullable: false, maxLength: 50),
                        Lastname = c.String(nullable: false, maxLength: 50),
                        Address = c.String(nullable: false, maxLength: 100),
                        Zipcode = c.String(nullable: false, maxLength: 4),
                        City = c.String(nullable: false, maxLength: 50),
                        Homepage = c.String(),
                        Attachment = c.Binary(nullable: false),
                        AttachmentType = c.String(nullable: false, maxLength: 5),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.EmployerJobPosition",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        EmployerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.JobPositions", t => t.Id)
                .ForeignKey("dbo.EmployerAccounts", t => t.EmployerId, cascadeDelete: true)
                .Index(t => t.Id)
                .Index(t => t.EmployerId);
            
            CreateTable(
                "dbo.RecruitmentAgencyJobPosition",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        RecruitmentAgencyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.JobPositions", t => t.Id)
                .ForeignKey("dbo.RecruitmentAgencyAccounts", t => t.RecruitmentAgencyId, cascadeDelete: true)
                .Index(t => t.Id)
                .Index(t => t.RecruitmentAgencyId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RecruitmentAgencyJobPosition", "RecruitmentAgencyId", "dbo.RecruitmentAgencyAccounts");
            DropForeignKey("dbo.RecruitmentAgencyJobPosition", "Id", "dbo.JobPositions");
            DropForeignKey("dbo.EmployerJobPosition", "EmployerId", "dbo.EmployerAccounts");
            DropForeignKey("dbo.EmployerJobPosition", "Id", "dbo.JobPositions");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.JobApplications", "JobSeekerId", "dbo.JobSeekerAccounts");
            DropForeignKey("dbo.JobSeekerAccounts", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.RecruitmentAgencyJobPositionContactPersons", "RecruitmentAgencyJobPositionId", "dbo.RecruitmentAgencyJobPosition");
            DropForeignKey("dbo.RecruitmentAgencyJobPositionContactPersons", "RecruitmentAgencyContactPersonId", "dbo.RecruitmentAgencyContactPersons");
            DropForeignKey("dbo.RecruitmentAgencyContactPersons", "RecruitmentAgencyId", "dbo.RecruitmentAgencyAccounts");
            DropForeignKey("dbo.RecruitmentAgencyAccounts", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.JobApplications", "JobPositionId", "dbo.JobPositions");
            DropForeignKey("dbo.EmployerJobPositionContactPersons", "EmployerJobPositionId", "dbo.EmployerJobPosition");
            DropForeignKey("dbo.EmployerJobPositionContactPersons", "EmployerContactPersonId", "dbo.EmployerContactPersons");
            DropForeignKey("dbo.EmployerContactPersons", "EmployerId", "dbo.EmployerAccounts");
            DropForeignKey("dbo.EmployerAccounts", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.RecruitmentAgencyJobPosition", new[] { "RecruitmentAgencyId" });
            DropIndex("dbo.RecruitmentAgencyJobPosition", new[] { "Id" });
            DropIndex("dbo.EmployerJobPosition", new[] { "EmployerId" });
            DropIndex("dbo.EmployerJobPosition", new[] { "Id" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.JobSeekerAccounts", new[] { "UserId" });
            DropIndex("dbo.RecruitmentAgencyAccounts", new[] { "UserId" });
            DropIndex("dbo.RecruitmentAgencyContactPersons", new[] { "RecruitmentAgencyId" });
            DropIndex("dbo.RecruitmentAgencyJobPositionContactPersons", new[] { "RecruitmentAgencyJobPositionId" });
            DropIndex("dbo.RecruitmentAgencyJobPositionContactPersons", new[] { "RecruitmentAgencyContactPersonId" });
            DropIndex("dbo.JobApplications", new[] { "JobSeekerId" });
            DropIndex("dbo.JobApplications", new[] { "JobPositionId" });
            DropIndex("dbo.EmployerJobPositionContactPersons", new[] { "EmployerJobPositionId" });
            DropIndex("dbo.EmployerJobPositionContactPersons", new[] { "EmployerContactPersonId" });
            DropIndex("dbo.EmployerContactPersons", new[] { "EmployerId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.EmployerAccounts", new[] { "UserId" });
            DropTable("dbo.RecruitmentAgencyJobPosition");
            DropTable("dbo.EmployerJobPosition");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.JobSeekerAccounts");
            DropTable("dbo.RecruitmentAgencyAccounts");
            DropTable("dbo.RecruitmentAgencyContactPersons");
            DropTable("dbo.RecruitmentAgencyJobPositionContactPersons");
            DropTable("dbo.JobApplications");
            DropTable("dbo.EmployerJobPositionContactPersons");
            DropTable("dbo.JobPositions");
            DropTable("dbo.EmployerContactPersons");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.EmployerAccounts");
        }
    }
}
