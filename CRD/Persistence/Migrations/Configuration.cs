using CRD.Persistence;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.Migrations;
using System.Linq;

namespace CRD.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            MigrationsDirectory = @"Persistence\Migrations";
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            var store = new RoleStore<IdentityRole>(context);
            var manager = new RoleManager<IdentityRole>(store);

            if (!context.Roles.Any(r => r.Name == "JobSeeker"))
            {
                var role = new IdentityRole { Name = "JobSeeker" };
                manager.Create(role);
            }

            if (!context.Roles.Any(r => r.Name == "Employer"))
            {
                var role = new IdentityRole { Name = "Employer" };
                manager.Create(role);
            }

            if (!context.Roles.Any(r => r.Name == "RecruitmentAgency"))
            {
                var role = new IdentityRole { Name = "RecruitmentAgency" };
                manager.Create(role);
            }
        }
    }
}
