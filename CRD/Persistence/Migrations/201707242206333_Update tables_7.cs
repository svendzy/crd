using System.Data.Entity.Migrations;

namespace CRD.Migrations
{
    public partial class Updatetables_7 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.JobSeekerCourses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FromDate = c.DateTime(nullable: false),
                        ToDate = c.DateTime(nullable: false),
                        Course = c.String(nullable: false, maxLength: 100),
                        Description = c.String(nullable: false, maxLength: 200),
                        JobSeekerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.JobSeekerAccounts", t => t.JobSeekerId, cascadeDelete: true)
                .Index(t => t.JobSeekerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.JobSeekerCourses", "JobSeekerId", "dbo.JobSeekerAccounts");
            DropIndex("dbo.JobSeekerCourses", new[] { "JobSeekerId" });
            DropTable("dbo.JobSeekerCourses");
        }
    }
}
