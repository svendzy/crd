using System.Data.Entity.Migrations;

namespace CRD.Migrations
{
    public partial class Updatetables_8 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.RecruitmentAgencyAccounts", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.EmployerAccounts", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.RecruitmentAgencyAccounts", new[] { "UserId" });
            DropIndex("dbo.EmployerAccounts", new[] { "UserId" });
            AlterColumn("dbo.RecruitmentAgencyAccounts", "UserId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.EmployerAccounts", "UserId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.RecruitmentAgencyAccounts", "UserId");
            CreateIndex("dbo.EmployerAccounts", "UserId");
            AddForeignKey("dbo.RecruitmentAgencyAccounts", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.EmployerAccounts", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EmployerAccounts", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.RecruitmentAgencyAccounts", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.EmployerAccounts", new[] { "UserId" });
            DropIndex("dbo.RecruitmentAgencyAccounts", new[] { "UserId" });
            AlterColumn("dbo.EmployerAccounts", "UserId", c => c.String(maxLength: 128));
            AlterColumn("dbo.RecruitmentAgencyAccounts", "UserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.EmployerAccounts", "UserId");
            CreateIndex("dbo.RecruitmentAgencyAccounts", "UserId");
            AddForeignKey("dbo.EmployerAccounts", "UserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.RecruitmentAgencyAccounts", "UserId", "dbo.AspNetUsers", "Id");
        }
    }
}
