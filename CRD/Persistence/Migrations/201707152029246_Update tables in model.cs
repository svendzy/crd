using System.Data.Entity.Migrations;

namespace CRD.Migrations
{
    public partial class Updatetablesinmodel : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.JobSeekerAccounts", "Homepage", c => c.String(nullable: false));
            DropColumn("dbo.JobSeekerAccounts", "Attachment");
            DropColumn("dbo.JobSeekerAccounts", "AttachmentType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.JobSeekerAccounts", "AttachmentType", c => c.String(nullable: false, maxLength: 5));
            AddColumn("dbo.JobSeekerAccounts", "Attachment", c => c.Binary(nullable: false));
            AlterColumn("dbo.JobSeekerAccounts", "Homepage", c => c.String());
        }
    }
}
