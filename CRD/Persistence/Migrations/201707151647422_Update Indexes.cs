using System.Data.Entity.Migrations;

namespace CRD.Migrations
{
    public partial class UpdateIndexes : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.EmployerAccounts", "CompanyName", unique: true, name: "IX_Em_CompanyName");
            CreateIndex("dbo.RecruitmentAgencyAccounts", "CompanyName", unique: true, name: "IX_Ra_CompanyName");
        }
        
        public override void Down()
        {
            DropIndex("dbo.RecruitmentAgencyAccounts", "IX_Ra_CompanyName");
            DropIndex("dbo.EmployerAccounts", "IX_Em_CompanyName");
        }
    }
}
