// <auto-generated />

using System.CodeDom.Compiler;
using System.Data.Entity.Migrations.Infrastructure;
using System.Resources;

namespace CRD.Migrations
{
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Updatetables_7 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Updatetables_7));
        
        string IMigrationMetadata.Id
        {
            get { return "201707242206333_Update tables_7"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
