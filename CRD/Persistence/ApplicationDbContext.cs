﻿using CRD.Core.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace CRD.Persistence
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IApplicationDbContext
    {
        public DbSet<JobSeekerAccount> JobSeekers { get; set; }
        public DbSet<JobSeekerEducation> JobSeekerEducation { get; set; }
        public DbSet<JobSeekerExperience> JobSeekerExperience { get; set; }
        public DbSet<JobSeekerCourse> JobSeekerCourses { get; set; }
        public DbSet<JobApplication> JobApplications { get; set; }
        public DbSet<EmployerJobPosition> EmployerJobPositions { get; set; }
        public DbSet<JobPosition> JobPositions { get; set; }
        public DbSet<EmployerAccount> Employers { get; set; }
        public DbSet<EmployerContactPerson> EmployerContactPersons { get; set; }
        public DbSet<RecruitmentAgencyAccount> RecruitmentAgencies { get; set; }
        public DbSet<RecruitmentAgencyJobPosition> RecruitmentAgencyJobPositions { get; set; }
        public DbSet<RecruitmentAgencyContactPerson> RecruitmentAgencyContactPersons { get; set; }

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}