﻿using CRD.Core;
using CRD.Core.Repositories;
using CRD.Persistence.Repositories;

namespace CRD.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;

            Employers = new EmployerAccountRepository(_context);
            EmployerContactPersons = new EmployerContactPersonRepository(_context);
            EmployerJobPositions = new EmployerJobPositionRepository(_context);
            JobApplications = new JobApplicationRepository(_context);
            JobSearches = new JobSearchRepository(_context);
            JobSeekers = new JobSeekerAccountRepository(_context);
            JobSeekerCourses = new JobSeekerCourseRepository(_context);
            JobSeekerEducations = new JobSeekerEducationRepository(_context);
            JobSeekerExperiences = new JobSeekerExperienceRepository(_context);
            RecruitmentAgencies = new RecruitmentAgencyAccountRepository(_context);
            RecruitmentAgencyContactPersons = new RecruitmentAgencyContactPersonRepository(_context);
            RecruitmentAgencyJobPositions = new RecruitmentAgencyJobPositionRepository(_context);
        }

        public IRecruitmentAgencyJobPositionRepository RecruitmentAgencyJobPositions { get; private set; }
        public IRecruitmentAgencyContactPersonRepository RecruitmentAgencyContactPersons { get; private set; }
        public IRecruitmentAgencyAccountRepository RecruitmentAgencies { get; private set; }
        public IJobSeekerExperienceRepository JobSeekerExperiences { get; private set; }
        public IJobSeekerEducationRepository JobSeekerEducations { get; private set; }
        public IJobSeekerCourseRepository JobSeekerCourses { get; private set; }
        public IJobSeekerAccountRepository JobSeekers { get; private set; }
        public IJobSearchRepository JobSearches { get; private set; }
        public IJobApplicationRepository JobApplications { get; private set; }
        public IEmployerJobPositionRepository EmployerJobPositions { get; private set; }
        public IEmployerContactPersonRepository EmployerContactPersons { get; private set; }
        public IEmployerAccountRepository Employers { get; private set; }


        public void Complete()
        {
            _context.SaveChanges();
        }
    }
}