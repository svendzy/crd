﻿using CRD.Core.Models;
using CRD.Core.Repositories;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace CRD.Persistence.Repositories
{
    public class JobSearchRepository : IJobSearchRepository
    {
        private readonly ApplicationDbContext _context;

        public JobSearchRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<RecruitmentAgencyJobPosition> SearchByRecruiters(int maxElementsToRetieve, string searchTerm)
        {
            return (string.IsNullOrEmpty(searchTerm)) ?
                 _context.RecruitmentAgencyJobPositions.Take(maxElementsToRetieve)
                    .Include(j => j.Contacts)
                    .Include(j => j.RecruitmentAgency)
                    .OrderByDescending(m => m.PostedDate)
                    .ToList() : _context.RecruitmentAgencyJobPositions.Where(j => j.JobDescription.Contains(searchTerm) || (j.JobTitle.Contains(searchTerm)))
                    .Take(maxElementsToRetieve)
                    .Include(j => j.Contacts)
                    .Include(j => j.RecruitmentAgency)
                    .OrderByDescending(m => m.PostedDate)
                    .ToList();
        }

        public IEnumerable<EmployerJobPosition> SearchByEmployers(int maxElementsToRetieve, string searchTerm)
        {
            return (string.IsNullOrEmpty(searchTerm)) ?
                _context.EmployerJobPositions.Take(maxElementsToRetieve)
                    .Include(j => j.Contacts)
                    .Include(j => j.Employer)
                    .OrderByDescending(m => m.PostedDate)
                    .ToList() :
                    _context.EmployerJobPositions.Where(j => j.JobDescription.Contains(searchTerm) || (j.JobTitle.Contains(searchTerm)))
                    .Take(maxElementsToRetieve)
                    .Include(j => j.Contacts)
                    .Include(j => j.Employer)
                    .OrderByDescending(m => m.PostedDate)
                    .ToList();
        }
    }
}