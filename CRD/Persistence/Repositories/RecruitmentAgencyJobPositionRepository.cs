﻿using CRD.Core.Models;
using CRD.Core.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace CRD.Persistence.Repositories
{
    internal class RecruitmentAgencyJobPositionRepository : IRecruitmentAgencyJobPositionRepository
    {
        private readonly ApplicationDbContext _context;

        public RecruitmentAgencyJobPositionRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<RecruitmentAgencyJobPosition> GetAll(int recruitmentAgencyId)
        {
            return _context.RecruitmentAgencyJobPositions
                .Where(j => j.RecruitmentAgencyId == recruitmentAgencyId)
                .OrderByDescending(j => j.PostedDate)
                .ToList();
        }

        public IEnumerable<RecruitmentAgencyJobPosition> GetLast10(int recruitmentAgencyId)
        {
            return _context.RecruitmentAgencyJobPositions
                .Where(j => j.RecruitmentAgencyId == recruitmentAgencyId)
                .OrderByDescending(j => j.PostedDate)
                .Take(10)
                .ToList();
        }

        public void Add(RecruitmentAgencyJobPosition recruitmentAgencyJobPosition)
        {
            _context.RecruitmentAgencyJobPositions.Add(recruitmentAgencyJobPosition);
        }

        public RecruitmentAgencyJobPosition Find(int id)
        {
            return _context.RecruitmentAgencyJobPositions.Find(id);
        }

        public void Remove(RecruitmentAgencyJobPosition recruitmentAgencyJobPosition)
        {
            _context.RecruitmentAgencyJobPositions.Remove(recruitmentAgencyJobPosition);
        }
    }
}