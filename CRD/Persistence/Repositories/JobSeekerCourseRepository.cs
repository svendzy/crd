﻿using CRD.Core.Models;
using CRD.Core.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace CRD.Persistence.Repositories
{
    public class JobSeekerCourseRepository : IJobSeekerCourseRepository
    {
        private readonly ApplicationDbContext _context;

        public JobSeekerCourseRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<JobSeekerCourse> GetAll(int jobSeekerId)
        {
            return _context.JobSeekerCourses.Where(c => c.JobSeekerId == jobSeekerId)
                .OrderByDescending(c => c.FromDate)
                .ToList();
        }

        public void Add(JobSeekerCourse jobSeekerCourse)
        {
            _context.JobSeekerCourses.Add(jobSeekerCourse);
        }

        public JobSeekerCourse Find(int id)
        {
            return _context.JobSeekerCourses.Find(id);
        }

        public void Remove(JobSeekerCourse jobSeekerCourse)
        {
            _context.JobSeekerCourses.Remove(jobSeekerCourse);
        }
    }
}