﻿using CRD.Core.Models;
using CRD.Core.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace CRD.Persistence.Repositories
{
    public class JobSeekerEducationRepository : IJobSeekerEducationRepository
    {
        private readonly ApplicationDbContext _context;

        public JobSeekerEducationRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<JobSeekerEducation> GetAll(int jobSeekerId)
        {
            return _context.JobSeekerEducation.Where(e => e.JobSeekerId == jobSeekerId)
                .OrderByDescending(e => e.FromDate)
                .ToList();
        }

        public void Add(JobSeekerEducation jobSeekerEducation)
        {
            _context.JobSeekerEducation.Add(jobSeekerEducation);
        }

        public JobSeekerEducation Find(int id)
        {
            return _context.JobSeekerEducation.Find(id);
        }

        public void Remove(JobSeekerEducation jobSeekerEducation)
        {
            _context.JobSeekerEducation.Remove(jobSeekerEducation);
        }
    }
}