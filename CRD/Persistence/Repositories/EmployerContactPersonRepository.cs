﻿using CRD.Core.Models;
using CRD.Core.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace CRD.Persistence.Repositories
{
    public class EmployerContactPersonRepository : IEmployerContactPersonRepository
    {
        private readonly ApplicationDbContext _context;

        public EmployerContactPersonRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<EmployerContactPerson> GetAll(int employerId)
        {
            return _context.EmployerContactPersons.Where(j => j.EmployerId == employerId).ToList();
        }

        public void Add(EmployerContactPerson contactPerson)
        {
            _context.EmployerContactPersons.Add(contactPerson);
        }

        public EmployerContactPerson Find(int id)
        {
            return _context.EmployerContactPersons.Find(id);
        }

        public void Remove(EmployerContactPerson contactPerson)
        {
            _context.EmployerContactPersons.Remove(contactPerson);
        }
    }
}