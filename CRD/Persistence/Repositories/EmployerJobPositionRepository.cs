﻿using CRD.Core.Models;
using CRD.Core.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace CRD.Persistence.Repositories
{
    public class EmployerJobPositionRepository : IEmployerJobPositionRepository
    {
        private readonly ApplicationDbContext _context;

        public EmployerJobPositionRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<EmployerJobPosition> GetAll(int employerId)
        {
            return _context.EmployerJobPositions
                .Where(j => j.EmployerId == employerId)
                .OrderByDescending(j => j.PostedDate)
                .ToList();
        }

        public IEnumerable<EmployerJobPosition> GetLast10(int employerId)
        {
            return _context.EmployerJobPositions
                .Where(j => j.EmployerId == employerId)
                .OrderByDescending(j => j.PostedDate)
                .Take(10)
                .ToList();
        }

        public void Add(EmployerJobPosition employerJobPosition)
        {
            _context.EmployerJobPositions.Add(employerJobPosition);
        }

        public EmployerJobPosition Find(int id)
        {
            return _context.EmployerJobPositions.Find(id);
        }

        public void Remove(EmployerJobPosition employerJobPosition)
        {
            _context.EmployerJobPositions.Remove(employerJobPosition);
        }
    }
}