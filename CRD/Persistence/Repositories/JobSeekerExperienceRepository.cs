﻿using CRD.Core.Models;
using CRD.Core.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace CRD.Persistence.Repositories
{
    internal class JobSeekerExperienceRepository : IJobSeekerExperienceRepository
    {
        private readonly ApplicationDbContext _context;

        public JobSeekerExperienceRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<JobSeekerExperience> GetAll(int jobSeekerId)
        {
            return _context.JobSeekerExperience.Where(c => c.JobSeekerId == jobSeekerId)
                .OrderByDescending(c => c.FromDate)
                .ToList();
        }

        public void Add(JobSeekerExperience jobSeekerExperience)
        {
            _context.JobSeekerExperience.Add(jobSeekerExperience);
        }

        public JobSeekerExperience Find(int id)
        {
            return _context.JobSeekerExperience.Find(id);
        }

        public void Remove(JobSeekerExperience jobSeekerExperience)
        {
            _context.JobSeekerExperience.Remove(jobSeekerExperience);
        }
    }
}