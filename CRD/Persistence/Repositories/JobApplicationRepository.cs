﻿using CRD.Core.Models;
using CRD.Core.Repositories;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace CRD.Persistence.Repositories
{
    public class JobApplicationRepository : IJobApplicationRepository
    {
        private readonly ApplicationDbContext _context;

        public JobApplicationRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<JobApplication> GetAll(int jobSeekerId)
        {
            var applications = _context.JobApplications
                .Where(m => m.JobSeekerId == jobSeekerId)
                .Include(m => m.JobPosition)
                .OrderByDescending(m => m.JobPosition.PostedDate)
                .ToList();

            foreach (var application in applications)
            {
                if (application.JobPosition is EmployerJobPosition)
                {
                    var employerJobPosition = (EmployerJobPosition)application.JobPosition;
                    employerJobPosition.Employer = new EmployerAccountRepository(_context)
                        .Get(employerJobPosition.EmployerId);
                }
                if (application.JobPosition is RecruitmentAgencyJobPosition)
                {
                    var recruitmentAgencyJobPosition = (RecruitmentAgencyJobPosition)application.JobPosition;
                    recruitmentAgencyJobPosition.RecruitmentAgency = new RecruitmentAgencyAccountRepository(_context)
                        .Get(recruitmentAgencyJobPosition.RecruitmentAgencyId);
                }
            }

            return applications;
        }

        public IEnumerable<JobApplication> GetLast10(int jobSeekerId)
        {
            var applications = _context.JobApplications
               .Where(m => m.JobSeekerId == jobSeekerId)
               .Include(m => m.JobPosition)
               .OrderByDescending(m => m.JobPosition.PostedDate)
               .Take(10)
               .ToList();

            foreach (var application in applications)
            {
                if (application.JobPosition is EmployerJobPosition)
                {
                    var employerJobPosition = (EmployerJobPosition)application.JobPosition;
                    employerJobPosition.Employer = new EmployerAccountRepository(_context)
                        .Get(employerJobPosition.EmployerId);
                }
                if (application.JobPosition is RecruitmentAgencyJobPosition)
                {
                    var recruitmentAgencyJobPosition = (RecruitmentAgencyJobPosition)application.JobPosition;
                    recruitmentAgencyJobPosition.RecruitmentAgency = new RecruitmentAgencyAccountRepository(_context)
                        .Get(recruitmentAgencyJobPosition.RecruitmentAgencyId);
                }
            }
            return applications;
        }

        public IList<int> GetAppliedJobPositionsIds(int jobSeekerId)
        {
            return _context.JobApplications
                .Where(a => a.JobSeekerId == jobSeekerId)
                .Select(a => a.JobPositionId)
                .ToList();
        }

        public void Add(JobApplication jobApplication)
        {
            _context.JobApplications.Add(jobApplication);
        }

        public JobApplication Find(int id)
        {
            return _context.JobApplications.Find(id);
        }

        public void Remove(JobApplication jobApplication)
        {
            _context.JobApplications.Remove(jobApplication);
        }
    }
}