﻿using CRD.Core.Models;
using CRD.Core.Repositories;
using System.Linq;
using System.Threading.Tasks;

namespace CRD.Persistence.Repositories
{
    public class JobSeekerAccountRepository : IJobSeekerAccountRepository
    {
        private readonly ApplicationDbContext _context;

        public JobSeekerAccountRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void Add(JobSeekerAccount jobSeekerAccount)
        {
            _context.JobSeekers.Add(jobSeekerAccount);
        }

        public async Task<JobSeekerAccount> FindAsync(int jobSeekerId)
        {
            return await _context.JobSeekers.FindAsync(jobSeekerId);
        }

        public JobSeekerAccount FindByAppUser(string userId)
        {
            return _context.JobSeekers.Single(m => m.UserId == userId);
        }
    }
}