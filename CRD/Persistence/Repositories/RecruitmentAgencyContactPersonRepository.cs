﻿using CRD.Core.Models;
using CRD.Core.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace CRD.Persistence.Repositories
{
    internal class RecruitmentAgencyContactPersonRepository : IRecruitmentAgencyContactPersonRepository
    {
        private ApplicationDbContext _context;

        public RecruitmentAgencyContactPersonRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<RecruitmentAgencyContactPerson> GetAll(int recruiterId)
        {
            return _context.RecruitmentAgencyContactPersons.Where(j => j.RecruitmentAgencyId == recruiterId).ToList();
        }

        public void Add(RecruitmentAgencyContactPerson contactPerson)
        {
            _context.RecruitmentAgencyContactPersons.Add(contactPerson);
        }

        public RecruitmentAgencyContactPerson Find(int id)
        {
            return _context.RecruitmentAgencyContactPersons.Find(id);
        }

        public void Remove(RecruitmentAgencyContactPerson contactPerson)
        {
            _context.RecruitmentAgencyContactPersons.Remove(contactPerson);
        }
    }
}