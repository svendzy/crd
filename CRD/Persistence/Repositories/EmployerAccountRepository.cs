﻿using CRD.Core.Models;
using CRD.Core.Repositories;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace CRD.Persistence.Repositories
{
    public class EmployerAccountRepository : IEmployerAccountRepository
    {
        private readonly ApplicationDbContext _context;

        public EmployerAccountRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<EmployerAccount> GetAll()
        {
            return _context.Employers.Include(m => m.ApplicationUser).ToList();
        }

        public void Add(EmployerAccount employerAccount)
        {
            _context.Employers.Add(employerAccount);
        }

        public EmployerAccount FindByAppUser(string userId)
        {
            return _context.Employers.Single(m => m.UserId == userId);
        }

        public EmployerAccount Get(int id)
        {
            return _context.Employers.Single(m => m.Id == id);
        }
    }
}