﻿using CRD.Core.Models;
using CRD.Core.Repositories;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace CRD.Persistence.Repositories
{
    public class RecruitmentAgencyAccountRepository : IRecruitmentAgencyAccountRepository
    {
        private ApplicationDbContext _context;

        public RecruitmentAgencyAccountRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void Add(RecruitmentAgencyAccount recruitmentAgencyAccount)
        {
            _context.RecruitmentAgencies.Add(recruitmentAgencyAccount);
        }

        public IEnumerable<RecruitmentAgencyAccount> GetAll()
        {
            return _context.RecruitmentAgencies.Include(m => m.ApplicationUser).ToList();
        }

        public RecruitmentAgencyAccount FindByAppUser(string userId)
        {
            return _context.RecruitmentAgencies.Single(m => m.UserId == userId);
        }

        public RecruitmentAgencyAccount Get(int id)
        {
            return _context.RecruitmentAgencies.Single(m => m.Id == id);
        }
    }
}