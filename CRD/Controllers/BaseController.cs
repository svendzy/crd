﻿using CRD.Core;
using CRD.Core.Attributes;
using CRD.Core.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Web;
using System.Web.Mvc;

namespace CRD.Controllers
{
    [CustomError]
    public class BaseController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public BaseController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public BaseController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        protected ApplicationSignInManager SignInManager => _signInManager ?? 
            HttpContext.GetOwinContext().Get<ApplicationSignInManager>();

        protected ApplicationUserManager UserManager => _userManager ?? 
            HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

        protected IUnitOfWork UnitOfWork => _unitOfWork;

        protected IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        protected ActionResult RedirectToLoginPage => RedirectToAction("Login", "Home");

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        protected bool IsJobSeekerLoggedIn(int jobSeekerId)
        {
            var loggedInJobSeeker = Session["User"] as JobSeekerAccount;
            if (loggedInJobSeeker?.Id != jobSeekerId) return false;
            return true;
        }

        protected bool IsEmployerLoggedIn(int employerId)
        {
            var loggedInEmployer = Session["User"] as EmployerAccount;
            if (loggedInEmployer?.Id != employerId) return false;
            return true;
        }

        protected bool IsRecruitmentAgencyLoggedIn(int recruitmentAgencyId)
        {
            var loggedInRecruitmentAgency = Session["User"] as RecruitmentAgencyAccount;
            if (loggedInRecruitmentAgency?.Id != recruitmentAgencyId) return false;
            return true;
        }
    }
}