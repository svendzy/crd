﻿using CRD.Core;
using CRD.Core.Attributes;
using CRD.Core.Models;
using CRD.Core.ViewModels;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRD.Controllers
{
    [CustomAuthorize(Roles = "Employer")]
    public class EmployerAccountController : BaseController
    {
        private readonly IAccountManager _accountManager;

        public EmployerAccountController(IUnitOfWork unitOfWork, IAccountManager accountManager) : base(unitOfWork)
        {
            _accountManager = accountManager;
        }

        // GET: /employer
        [Route("employer")]
        public ActionResult Index()
        {
            var employer = Session["User"] as EmployerAccount;
            if (employer == null)
                return RedirectToLoginPage;

            var jobPositions = UnitOfWork.EmployerJobPositions.GetLast10(employer.Id);

            var viewModel = jobPositions.Select(j => new EmployerJobPositionViewModel
            {
                Id = j.Id,
                JobTitle = j.JobTitle,
                JobDescription = j.JobDescription,
                PostedDate = j.PostedDate.ToString("dd.MM.yyyy"),
                ExpiryDate = j.ExpiryDate.ToString("dd.MM.yyyy")
            });

            return View(viewModel);
        }

        // GET: /employer/create
        [AllowAnonymous]
        [Route("employer/create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: /employer/create
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Route("employer/create")]
        public async Task<ActionResult> Create(EmployerAccountViewModel model)
        {
            if (!ModelState.IsValid) return View(model);

            var employerAccount = await _accountManager.CreateEmployerAccount(model.Email, model.Password,
                new EmployerAccount()
                {
                    CompanyName = model.CompanyName,
                    UserId = ""
                });

            await SignInManager.SignInAsync(employerAccount.ApplicationUser, isPersistent: false, rememberBrowser: false);

            Session["User"] = employerAccount;
            Session["UserRole"] = "Employer";

            return RedirectToAction("Index", "EmployerAccount");
        }

        // GET: /employer/recruiters
        [Route("employer/recruiters")]
        public ActionResult Recruiters()
        {
            var recruiters = UnitOfWork.RecruitmentAgencies.GetAll();
            var viewModel = recruiters
                .Select(r => new RecruiterViewModel { CompanyName = r.CompanyName, Email = r.ApplicationUser.Email});

            return View(viewModel);
        }
    }
}