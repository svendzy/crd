﻿using CRD.Core;
using CRD.Core.Attributes;
using CRD.Core.Models;
using CRD.Core.ViewModels;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRD.Controllers
{
    [CustomAuthorize(Roles = "RecruitmentAgency")]
    public class RecruitmentAgencyAccountController : BaseController
    {
        private readonly IAccountManager _accountManager;

        public RecruitmentAgencyAccountController(IUnitOfWork unitOfWork, IAccountManager accountManager) : base(unitOfWork)
        {
            _accountManager = accountManager;
        }

        //
        // GET: /recruiter
        [Route("recruiter")]
        public ActionResult Index()
        {
            var recruitmentAgency = Session["User"] as RecruitmentAgencyAccount;
            if (recruitmentAgency == null)
                return RedirectToLoginPage;

            var jobPositions = UnitOfWork.RecruitmentAgencyJobPositions.GetLast10(recruitmentAgency.Id);

            var viewModel = jobPositions.Select(j => new RecruitmentAgencyJobPositionViewModel
            {
                Id = j.Id,
                JobTitle = j.JobTitle,
                JobDescription = j.JobDescription,
                PostedDate = j.PostedDate.ToString("dd.MM.yyyy"),
                ExpiryDate = j.ExpiryDate.ToString("dd.MM.yyyy")
            });

            return View(viewModel);
        }

        //
        // GET: /recruiter/create
        [AllowAnonymous]
        [Route("recruiter/create")]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /recruiter/create
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Route("recruiter/create")]
        public async Task<ActionResult> Create(RecruitmentAgencyAccountViewModel model)
        {
            if (!ModelState.IsValid) return View(model);

            var recruitmentAgencyAccount = await _accountManager.CreateRecruitmentAgencyAccount(model.Email, model.Password,
                new RecruitmentAgencyAccount()
                {
                    CompanyName = model.CompanyName,
                    UserId = ""
                });

            await SignInManager.SignInAsync(recruitmentAgencyAccount.ApplicationUser, isPersistent: false, rememberBrowser: false);

            Session["User"] = recruitmentAgencyAccount;
            Session["UserRole"] = "RecruitmentAgency";

            return RedirectToAction("Index", "RecruitmentAgencyAccount");
        }

        // GET: /recruiter/employers
        [Route("recruiter/employers")]
        public ActionResult Employers()
        {
            var employers = UnitOfWork.Employers.GetAll();
            var viewModel = employers
                .Select(r => new EmployerViewModel { CompanyName = r.CompanyName, Email = r.ApplicationUser.Email });
                
            return View(viewModel);
        }
    }
}