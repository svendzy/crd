﻿using CRD.Core;
using CRD.Core.Attributes;
using CRD.Core.Models;
using CRD.Core.ViewModels;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRD.Controllers
{
    [CustomAuthorize(Roles = "JobSeeker")]
    public class JobSeekerAccountController : BaseController
    {
        private readonly IAccountManager _accountManager;

        public JobSeekerAccountController(IUnitOfWork unitOfWork, IAccountManager accountManager) : base(unitOfWork)
        {
            _accountManager = accountManager;
        }

        //
        // GET: /jobseeker
        [Route("jobseeker")]
        public ActionResult Index()
        {
            var jobSeeker = Session["User"] as JobSeekerAccount;
            if (jobSeeker == null)
                return RedirectToLoginPage;

            var jobApplications = UnitOfWork.JobApplications.GetLast10(jobSeeker.Id);

            var viewModel = jobApplications.Select(m => new JobApplicationViewModel()
            {
                Id = m.Id,
                JobPosition = (m.JobPosition is EmployerJobPosition) ? (JobPositionViewModel)
                    new EmployerJobPositionViewModel(m.JobPosition as EmployerJobPosition) :
                    new RecruitmentAgencyJobPositionViewModel(m.JobPosition as RecruitmentAgencyJobPosition),
                Text = m.Text,
            });

            return View(viewModel);
        }

        //
        // GET: /jobseeker/create

        [AllowAnonymous]
        [Route("jobseeker/create")]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /jobseeker/create

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Route("jobseeker/create")]
        public async Task<ActionResult> Create(JobSeekerAccountViewModel model)
        {
            if (!ModelState.IsValid) return View(model);

            var jobSeekerAccount = await _accountManager.CreateJobSeekerAccount(model.Email, model.Password,
                new JobSeekerAccount()
                {
                    Firstname = model.Firstname,
                    Lastname = model.Lastname,
                    Address = model.Address,
                    Zipcode = model.Zipcode,
                    City = model.City,
                    Homepage = model.Homepage ?? "",
                    UserId = ""
                });

            await SignInManager.SignInAsync(jobSeekerAccount.ApplicationUser, isPersistent: false, rememberBrowser: false);

            Session["User"] = jobSeekerAccount;
            Session["UserRole"] = "JobSeeker";

            return RedirectToAction("Index", "JobSeekerAccount");
        }

        //
        // GET: /jobseeker
        [HttpGet]
        [Route("jobseeker/settings")]
        public async Task<ActionResult> Edit()
        {
            var jobSeeker = Session["User"] as JobSeekerAccount;
            if (jobSeeker == null)
                return RedirectToLoginPage;

            var jobSeekerAccount = await UnitOfWork.JobSeekers.FindAsync(jobSeeker.Id);
            if (jobSeekerAccount == null)
                return RedirectToLoginPage;

            var viewModel = new JobSeekerAccountSettingsViewModel
            {
                Firstname = jobSeekerAccount.Firstname,
                Lastname = jobSeekerAccount.Lastname,
                Address = jobSeekerAccount.Address,
                Zipcode = jobSeekerAccount.Zipcode,
                City = jobSeekerAccount.City,
                Homepage = jobSeekerAccount.Homepage
            };

            return View(viewModel);
        }

        //
        // GET: /jobseeker
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("jobseeker/settings")]
        public async Task<ActionResult> Edit(JobSeekerAccountSettingsViewModel model)
        {
            if (!ModelState.IsValid) return View(model);
            var jobSeeker = Session["User"] as JobSeekerAccount;
            if (jobSeeker == null)
                return RedirectToLoginPage;

            var jobSeekerAccount = await UnitOfWork.JobSeekers.FindAsync(jobSeeker.Id);
            if (jobSeekerAccount == null)
                return RedirectToLoginPage;

            jobSeekerAccount.Firstname = model.Firstname;
            jobSeekerAccount.Lastname = model.Lastname;
            jobSeekerAccount.Address = model.Address;
            jobSeekerAccount.Zipcode = model.Zipcode;
            jobSeekerAccount.City = model.City;
            jobSeekerAccount.Homepage = model.Homepage;

            UnitOfWork.Complete();

            return RedirectToAction("Index", "JobSeekerAccount");
        }
    }
}