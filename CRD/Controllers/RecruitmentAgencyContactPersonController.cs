﻿using CRD.Core;
using CRD.Core.Attributes;
using CRD.Core.Models;
using CRD.Core.ViewModels;
using System.Linq;
using System.Web.Mvc;

namespace CRD.Controllers
{
    [CustomAuthorize(Roles = "RecruitmentAgency")]
    public class RecruitmentAgencyContactPersonController : BaseController
    {
        public RecruitmentAgencyContactPersonController(
            IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        // GET: /recruiter/contacts
        [HttpGet]
        [Route("recruiter/contacts")]
        public ActionResult Index()
        {
            var recruiter = Session["User"] as RecruitmentAgencyAccount;
            if (recruiter == null)
                return RedirectToLoginPage;

            var contactPersons = UnitOfWork.RecruitmentAgencyContactPersons.GetAll(recruiter.Id);

            var viewModel = contactPersons.Select(j => new RecruitmentAgencyContactPersonViewModel()
            {
                Id = j.Id,
                Name = j.Name,
                Title = j.Title,
                Email = j.Email,
                Phone = j.Phone
            });

            return View(viewModel);
        }

        // GET: /recruiter/contacts/create
        [HttpGet]
        [Route("recruiter/contacts/create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: /recruiter/contacts/create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("recruiter/contacts/create")]
        public ActionResult Create(RecruitmentAgencyContactPersonViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var recruiter = Session["User"] as RecruitmentAgencyAccount;
            if (recruiter == null)
                return RedirectToLoginPage;

            var contactPerson = new RecruitmentAgencyContactPerson()
            {
                Id = model.Id,
                Name = model.Name,
                Title = model.Title,
                Email = model.Email,
                Phone = model.Phone,
                RecruitmentAgencyId = recruiter.Id
            };

            UnitOfWork.RecruitmentAgencyContactPersons.Add(contactPerson);
            UnitOfWork.Complete();

            return RedirectToAction("Index", "RecruitmentAgencyContactPerson");
        }

        // GET: /recruiter/contacts/{id}/edit
        [HttpGet]
        [Route("recruiter/contacts/{id}/edit")]
        public ActionResult Edit(int id)
        {
            var contactPerson = UnitOfWork.RecruitmentAgencyContactPersons.Find(id);
            if (contactPerson == null)
                return HttpNotFound();

            if (!IsRecruitmentAgencyLoggedIn(contactPerson.RecruitmentAgencyId))
                return RedirectToLoginPage;

            var viewModel = new RecruitmentAgencyContactPersonViewModel()
            {
                Id = contactPerson.Id,
                Name = contactPerson.Name,
                Title = contactPerson.Title,
                Email = contactPerson.Email,
                Phone = contactPerson.Phone
            };
            return View(viewModel);
        }

        // POST: /recruiter/contacts/{id}/edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("recruiter/contacts/{id}/edit")]
        public ActionResult Edit(int id, RecruitmentAgencyContactPersonViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var contactPerson = UnitOfWork.RecruitmentAgencyContactPersons.Find(id);
            if (contactPerson == null)
                return HttpNotFound();

            if (!IsRecruitmentAgencyLoggedIn(contactPerson.RecruitmentAgencyId))
                return RedirectToLoginPage;

            contactPerson.Name = model.Name;
            contactPerson.Title = model.Title;
            contactPerson.Email = model.Email;
            contactPerson.Phone = model.Phone;

            UnitOfWork.Complete();

            return RedirectToAction("Index", "RecruitmentAgencyContactPerson");
        }

        // POST: /recruiter/contacts/{id}/edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("recruiter/contacts/{id}/delete")]
        public ActionResult Delete(int id)
        {
            var contactPerson = UnitOfWork.RecruitmentAgencyContactPersons.Find(id);
            if (contactPerson == null)
                return HttpNotFound();

            if (!IsRecruitmentAgencyLoggedIn(contactPerson.RecruitmentAgencyId))
                return RedirectToLoginPage;

            UnitOfWork.RecruitmentAgencyContactPersons.Remove(contactPerson);
            UnitOfWork.Complete();

            return RedirectToAction("Index", "RecruitmentAgencyContactPerson");
        }
    }
}