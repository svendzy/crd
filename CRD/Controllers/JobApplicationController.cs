﻿using CRD.Core;
using CRD.Core.Attributes;
using CRD.Core.Models;
using CRD.Core.ViewModels;
using System.Linq;
using System.Web.Mvc;

namespace CRD.Controllers
{
    [CustomAuthorize(Roles = "JobSeeker")]
    public class JobApplicationController : BaseController
    {

        public JobApplicationController(
            IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        // GET: /jobseeker/positions/applications
        [HttpGet]
        [Route("jobseeker/positions/applications")]
        public ActionResult Index()
        {
            var jobSeeker = Session["User"] as JobSeekerAccount;
            if (jobSeeker == null)
                return RedirectToLoginPage;

            var jobApplications = UnitOfWork.JobApplications.GetAll(jobSeeker.Id);

            var viewModel = jobApplications.Select(m => new JobApplicationViewModel()
            {
                Id = m.Id,
                JobPosition = (m.JobPosition is EmployerJobPosition) ? (JobPositionViewModel) 
                    new EmployerJobPositionViewModel(m.JobPosition as EmployerJobPosition) : 
                    new RecruitmentAgencyJobPositionViewModel(m.JobPosition as RecruitmentAgencyJobPosition),
                Text = m.Text,
            });

            return View(viewModel);
        }

        // GET: /jobseeker/positions/{jobPositionId}/applications/create
        [HttpGet]
        [Route("jobseeker/positions/{jobPositionId}/applications/create")]
        public ActionResult Create(int jobPositionId)
        {
            return View();
        }

        // POST: /jobseeker/positions/{jobPositionId}/applications/create     
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("jobseeker/positions/{jobPositionId}/applications/create")]
        public ActionResult Create(int jobPositionId, JobApplicationViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var jobSeeker = Session["User"] as JobSeekerAccount;
            if (jobSeeker == null)
                return RedirectToLoginPage;

            var jobApplication = new JobApplication
            {
                Text = model.Text,
                JobPositionId = jobPositionId,
                JobSeekerId = jobSeeker.Id
            };

            UnitOfWork.JobApplications.Add(jobApplication);
            UnitOfWork.Complete();

            return RedirectToAction("Index", "JobApplication");
        }

        // GET: /jobseeker/positions/applications/edit/{id}
        [HttpGet]
        [Route("jobseeker/positions/applications/{id}/edit")]
        public ActionResult Edit(int id)
        {
            var jobApplication = UnitOfWork.JobApplications.Find(id);
            if (jobApplication == null)
                return HttpNotFound();

            if (!IsJobSeekerLoggedIn(jobApplication.JobSeekerId))
                return RedirectToLoginPage;

            var viewModel = new JobApplicationViewModel()
            {
                Text = jobApplication.Text
            };
            return View(viewModel);
        }

        // POST: /jobseeker/positions/applications/edit/{id}
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("jobseeker/positions/applications/{id}/edit")]
        public ActionResult Edit(int id, JobApplicationViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var jobApplication = UnitOfWork.JobApplications.Find(id);
            if (jobApplication == null)
                return HttpNotFound();

            if (!IsJobSeekerLoggedIn(jobApplication.JobSeekerId))
                return RedirectToLoginPage;

            jobApplication.Text = model.Text;

            UnitOfWork.Complete();

            return RedirectToAction("Index", "JobApplication");
        }

        // GET: /jobseeker/positions/applications/delete/{id}
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("jobseeker/positions/applications/{id}/delete")]
        public ActionResult Delete(int id)
        {
            var jobApplication = UnitOfWork.JobApplications.Find(id);
            if (jobApplication == null)
                return HttpNotFound();

            if (!IsJobSeekerLoggedIn(jobApplication.JobSeekerId))
                return RedirectToLoginPage;

            UnitOfWork.JobApplications.Remove(jobApplication);
            UnitOfWork.Complete();

            return RedirectToAction("Index", "JobApplication");
        }
    }
}