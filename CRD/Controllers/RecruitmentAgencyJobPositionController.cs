﻿using CRD.Core;
using CRD.Core.Attributes;
using CRD.Core.Models;
using CRD.Core.ViewModels;
using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace CRD.Controllers
{
    [CustomAuthorize(Roles = "RecruitmentAgency")]
    public class RecruitmentAgencyJobPositionController : BaseController
    {

        public RecruitmentAgencyJobPositionController(
            IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        // GET: /recruiter/positions
        [HttpGet]
        [Route("recruiter/positions")]
        public ActionResult Index()
        {
            var recruitmentAgency = Session["User"] as RecruitmentAgencyAccount;
            if (recruitmentAgency == null)
                return RedirectToLoginPage;

            var jobPositions = UnitOfWork.RecruitmentAgencyJobPositions.GetAll(recruitmentAgency.Id);

            var viewModel = jobPositions.Select(j => new RecruitmentAgencyJobPositionViewModel
            {
                Id = j.Id,
                JobTitle = j.JobTitle,
                JobDescription = j.JobDescription,
                PostedDate = j.PostedDate.ToString("dd.MM.yyyy"),
                ExpiryDate = j.ExpiryDate.ToString("dd.MM.yyyy")
            });

            return View(viewModel);
        }

        // GET: /recruiter/positions/create
        [HttpGet]
        [Route("recruiter/positions/create")]
        public ActionResult Create()
        {
            var viewModel = new RecruitmentAgencyJobPositionViewModel
            {
                JobTitle = "",
                JobDescription = "",
                ExpiryDate = DateTime.Now.ToString("dd.MM.yyyy")
            };

            return View(viewModel);
        }

        // POST: /recruiter/positions/create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("recruiter/positions/create")]
        public ActionResult Create(RecruitmentAgencyJobPositionViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var recruitmentAgency = Session["User"] as RecruitmentAgencyAccount;
            if (recruitmentAgency == null)
                return RedirectToLoginPage;

            var recruitmentAgencyJobPosition = new RecruitmentAgencyJobPosition
            {
                JobTitle = model.JobTitle,
                JobDescription = model.JobDescription,
                PostedDate = DateTime.Now,
                ExpiryDate = DateTime.ParseExact(model.ExpiryDate, "dd.MM.yyyy", CultureInfo.InvariantCulture),
                RecruitmentAgencyId = recruitmentAgency.Id
            };
            UnitOfWork.RecruitmentAgencyJobPositions.Add(recruitmentAgencyJobPosition);
            UnitOfWork.Complete();

            return RedirectToAction("Index", "RecruitmentAgencyJobPosition");
        }

        // GET: /recruiter/positions/{id}/edit
        [HttpGet]
        [Route("recruiter/positions/{id}/edit")]
        public ActionResult Edit(int id)
        {
            var recruitmentAgencyJobPosition = UnitOfWork.RecruitmentAgencyJobPositions.Find(id);
            if (recruitmentAgencyJobPosition == null)
                return HttpNotFound();

            if (!IsRecruitmentAgencyLoggedIn(recruitmentAgencyJobPosition.RecruitmentAgencyId))
                return RedirectToLoginPage;

            var viewModel = new RecruitmentAgencyJobPositionViewModel
            {
                JobTitle = recruitmentAgencyJobPosition.JobTitle,
                JobDescription = recruitmentAgencyJobPosition.JobDescription,
                ExpiryDate = recruitmentAgencyJobPosition.ExpiryDate.ToString("dd.MM.yyyy")
            };
            return View(viewModel);
        }

        // POST: /recruiter/positions/{id}/edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("recruiter/positions/{id}/edit")]
        public ActionResult Edit(int id, RecruitmentAgencyJobPositionViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var recruitmentAgencyJobPosition = UnitOfWork.RecruitmentAgencyJobPositions.Find(id);
            if (recruitmentAgencyJobPosition == null)
                return HttpNotFound();

            if (!IsRecruitmentAgencyLoggedIn(recruitmentAgencyJobPosition.RecruitmentAgencyId))
                return RedirectToLoginPage;

            recruitmentAgencyJobPosition.JobTitle = model.JobTitle;
            recruitmentAgencyJobPosition.JobDescription = model.JobDescription;
            recruitmentAgencyJobPosition.ExpiryDate = DateTime.ParseExact(model.ExpiryDate, "dd.MM.yyyy", CultureInfo.InvariantCulture);

            UnitOfWork.Complete();

            return RedirectToAction("Index", "RecruitmentAgencyJobPosition");
        }

        // POST: /recruiter/positions/{id}/delete
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("recruiter/positions/{id}/delete")]
        public ActionResult Delete(int id)
        {
            var recruitmentAgencyJobPosition = UnitOfWork.RecruitmentAgencyJobPositions.Find(id);
            if (recruitmentAgencyJobPosition == null)
                return HttpNotFound();

            if (!IsRecruitmentAgencyLoggedIn(recruitmentAgencyJobPosition.RecruitmentAgencyId))
                return RedirectToLoginPage;

            UnitOfWork.RecruitmentAgencyJobPositions.Remove(recruitmentAgencyJobPosition);
            UnitOfWork.Complete();

            return RedirectToAction("Index", "RecruitmentAgencyJobPosition");
        }
    }
}