﻿using CRD.Core;
using CRD.Core.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CRD.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(
            IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public HomeController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
            : base(userManager, signInManager)
        {
        }

        // GET: /
        [Route("")]
        public ActionResult Index()
        {
            return View();
        }

        // GET: /about
        [Route("about")]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        // GET: /login  
        [AllowAnonymous]
        [Route("login")]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        // POST: /login     
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Route("login")]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    var user = await UserManager.FindByEmailAsync(model.Email);
                    var userRoles = await UserManager.GetRolesAsync(user.Id);

                    if (userRoles.Contains("JobSeeker"))
                    {
                        var jobSeeker = UnitOfWork.JobSeekers.FindByAppUser(user.Id);

                        Session["User"] = jobSeeker;
                        Session["UserRole"] = "JobSeeker";

                        return RedirectToAction("Index", "JobSeekerAccount");
                    }
                    else if (userRoles.Contains("Employer"))
                    {
                        var employer = UnitOfWork.Employers.FindByAppUser(user.Id);

                        Session["User"] = employer;
                        Session["UserRole"] = "Employer";

                        return RedirectToAction("Index", "EmployerAccount");
                    }
                    else if (userRoles.Contains("RecruitmentAgency"))
                    {
                        var recruitmentAgency = UnitOfWork.RecruitmentAgencies.FindByAppUser(user.Id);

                        Session["User"] = recruitmentAgency;
                        Session["UserRole"] = "RecruitmentAgency";

                        return RedirectToAction("Index", "RecruitmentAgencyAccount");
                    }
                    else
                        return RedirectToAction("Index", "Home");
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        // POST: /logOff    
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("logoff")]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            Session.Abandon();
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            return RedirectToAction("Index", "Home");
        }

        // GET: /contact    
        [Route("contact")]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}