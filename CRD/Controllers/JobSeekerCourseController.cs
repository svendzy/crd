﻿using CRD.Core;
using CRD.Core.Attributes;
using CRD.Core.Models;
using CRD.Core.ViewModels;
using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace CRD.Controllers
{
    [CustomAuthorize(Roles = "JobSeeker")]
    public class JobSeekerCourseController : BaseController
    {
        public JobSeekerCourseController(
            IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        // GET: /jobseeker/courses
        [HttpGet]
        [Route("jobseeker/courses")]
        public ActionResult Index()
        {
            var jobSeeker = Session["User"] as JobSeekerAccount;

            var courses = UnitOfWork.JobSeekerCourses.GetAll(jobSeeker.Id);

            var viewModel = courses.Select(c => new JobSeekerCourseViewModel()
            {
                    Id = c.Id,
                    FromDate = c.FromDate.ToString("dd.MM.yyyy"),
                    ToDate = c.ToDate.ToString("dd.MM.yyyy"),
                    Course = c.Course,
                    Description = c.Description
            });

            return View(viewModel);
        }

        // GET: /jobseeker/courses/create
        [HttpGet]
        [Route("jobseeker/courses/create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: /jobseeker/courses/create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("jobseeker/courses/create")]
        public ActionResult Create(JobSeekerCourseViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var jobSeeker = Session["User"] as JobSeekerAccount;
            if (jobSeeker == null)
                return RedirectToLoginPage;

            var jobSeekerCourse = new JobSeekerCourse
            {
                FromDate = DateTime.ParseExact(model.FromDate, "dd.MM.yyyy", CultureInfo.InvariantCulture),
                ToDate = DateTime.ParseExact(model.ToDate, "dd.MM.yyyy", CultureInfo.InvariantCulture),
                Course = model.Course,
                Description = model.Description,
                JobSeekerId = jobSeeker.Id
            };
            UnitOfWork.JobSeekerCourses.Add(jobSeekerCourse);
            UnitOfWork.Complete();

            return RedirectToAction("Index", "JobSeekerCourse");
        }

        // GET: /jobseeker/courses/{id}/edit
        [HttpGet]
        [Route("jobseeker/courses/{id}/edit")]
        public ActionResult Edit(int id)
        {
            var jobSeekerCourse = UnitOfWork.JobSeekerCourses.Find(id);
            if (jobSeekerCourse == null)
                return HttpNotFound();

            if (!IsJobSeekerLoggedIn(jobSeekerCourse.JobSeekerId))
                return RedirectToLoginPage;

            var viewModel = new JobSeekerCourseViewModel
            {
                Id = jobSeekerCourse.Id,
                FromDate = jobSeekerCourse.FromDate.ToString("dd.MM.yyyy"),
                ToDate =  jobSeekerCourse.ToDate.ToString("dd.MM.yyyy"),
                Course = jobSeekerCourse.Course,
                Description = jobSeekerCourse.Description
            };
            return View(viewModel);
        }


        // POST: /jobseeker/courses/{id}/edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("jobseeker/courses/{id}/edit")]
        public ActionResult Edit(int id, JobSeekerCourseViewModel model)
        {
            var jobSeekerCourse = UnitOfWork.JobSeekerCourses.Find(id);
            if (jobSeekerCourse == null)
                return HttpNotFound();

            if (!IsJobSeekerLoggedIn(jobSeekerCourse.JobSeekerId))
                return RedirectToLoginPage;

            jobSeekerCourse.FromDate = DateTime.ParseExact(model.FromDate, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            jobSeekerCourse.ToDate = DateTime.ParseExact(model.ToDate, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            jobSeekerCourse.Course = model.Course;
            jobSeekerCourse.Description = model.Description;

            UnitOfWork.Complete();

            return RedirectToAction("Index", "JobSeekerCourse");
        }

        // POST: /jobseeker/courses/{id}/delete
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("jobseeker/courses/{id}/delete")]
        public ActionResult Delete(int id)
        {
            var jobSeekerCourse = UnitOfWork.JobSeekerCourses.Find(id);
            if (jobSeekerCourse == null)
                return HttpNotFound();

            if (!IsJobSeekerLoggedIn(jobSeekerCourse.JobSeekerId))
                return RedirectToLoginPage;

            UnitOfWork.JobSeekerCourses.Remove(jobSeekerCourse);
            UnitOfWork.Complete();

            return RedirectToAction("Index", "JobSeekerCourse");
        }
    }
}