﻿using CRD.Core;
using CRD.Core.Attributes;
using CRD.Core.Models;
using CRD.Core.ViewModels;
using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace CRD.Controllers
{
    [CustomAuthorize(Roles = "JobSeeker")]
    public class JobSeekerEducationController : BaseController
    {

        public JobSeekerEducationController(
            IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        // GET: /jobseeker/educations
        [Route("jobseeker/educations")]
        public ActionResult Index()
        {
            var jobSeeker = Session["User"] as JobSeekerAccount;

            var education = UnitOfWork.JobSeekerEducations.GetAll(jobSeeker.Id);

            var viewModel = education.Select(c => new JobSeekerEducationViewModel()
            {
                Id = c.Id,
                FromDate = c.FromDate.ToString("dd.MM.yyyy"),
                ToDate = c.ToDate.ToString("dd.MM.yyyy"),
                Place = c.Place,
                Description = c.Description
            });

            return View(viewModel);
        }

        // GET: /jobseeker/educations/create
        [Route("jobseeker/educations/create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: /jobseeker/educations/create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("jobseeker/educations/create")]
        public ActionResult Create(JobSeekerEducationViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var jobSeeker = Session["User"] as JobSeekerAccount;
            if (jobSeeker == null)
                return RedirectToLoginPage;

            var jobSeekerEducation = new JobSeekerEducation
            {
                FromDate = DateTime.ParseExact(model.FromDate, "dd.MM.yyyy", CultureInfo.InvariantCulture),
                ToDate = DateTime.ParseExact(model.ToDate, "dd.MM.yyyy", CultureInfo.InvariantCulture),
                Place = model.Place,
                Description = model.Description,
                JobSeekerId = jobSeeker.Id
            };
            UnitOfWork.JobSeekerEducations.Add(jobSeekerEducation);
            UnitOfWork.Complete();

            return RedirectToAction("Index", "JobSeekerEducation");
        }

        // GET: /jobseeker/educations/{id}/edit
        [Route("jobseeker/educations/{id}/edit")]
        public ActionResult Edit(int id)
        {
            var jobSeekerEducation = UnitOfWork.JobSeekerEducations.Find(id);
            if (jobSeekerEducation == null)
                return HttpNotFound();

            if (!IsJobSeekerLoggedIn(jobSeekerEducation.JobSeekerId))
                return RedirectToLoginPage;

            var viewModel = new JobSeekerEducationViewModel
            {
                Id = jobSeekerEducation.Id,
                FromDate = jobSeekerEducation.FromDate.ToString("dd.MM.yyyy"),
                ToDate = jobSeekerEducation.ToDate.ToString("dd.MM.yyyy"),
                Place = jobSeekerEducation.Place,
                Description = jobSeekerEducation.Description
            };
            return View(viewModel);
        }


        // POST: /jobseeker/educations/{id}/edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("jobseeker/educations/{id}/edit")]
        public ActionResult Edit(int id, JobSeekerEducationViewModel model)
        {
            var jobSeekerEducation = UnitOfWork.JobSeekerEducations.Find(id);
            if (jobSeekerEducation == null)
                return HttpNotFound();

            if (!base.IsJobSeekerLoggedIn(jobSeekerEducation.JobSeekerId))
                return RedirectToLoginPage;

            jobSeekerEducation.FromDate = DateTime.ParseExact(model.FromDate, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            jobSeekerEducation.ToDate = DateTime.ParseExact(model.ToDate, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            jobSeekerEducation.Place = model.Place;
            jobSeekerEducation.Description = model.Description;

            UnitOfWork.Complete();

            return RedirectToAction("Index", "JobSeekerEducation");
        }

        // POST: /jobseeker/educations/{id}/delete
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("jobseeker/educations/{id}/delete")]
        public ActionResult Delete(int id)
        {
            var jobSeekerEducation = UnitOfWork.JobSeekerEducations.Find(id);
            if (jobSeekerEducation == null)
                return HttpNotFound();

            if (!IsJobSeekerLoggedIn(jobSeekerEducation.JobSeekerId))
                return RedirectToLoginPage;

            UnitOfWork.JobSeekerEducations.Remove(jobSeekerEducation);
            UnitOfWork.Complete();

            return RedirectToAction("Index", "JobSeekerEducation");
        }
    }
}