﻿using CRD.Core;
using CRD.Core.Attributes;
using CRD.Core.Models;
using CRD.Core.ViewModels;
using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace CRD.Controllers
{
    [CustomAuthorize(Roles = "Employer")]
    public class EmployerJobPositionController : BaseController
    {
        public EmployerJobPositionController(
            IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        // GET: /employer/positions
        [Route("employer/positions")]
        public ActionResult Index()
        {
            var employer = Session["User"] as EmployerAccount;
            if (employer == null)
                return RedirectToLoginPage;

            var jobPositions = UnitOfWork.EmployerJobPositions.GetAll(employer.Id);

            var viewModel = jobPositions.Select(j => new EmployerJobPositionViewModel
            {
                Id = j.Id,
                JobTitle = j.JobTitle,
                JobDescription = j.JobDescription,
                PostedDate = j.PostedDate.ToString("dd.MM.yyyy"),
                ExpiryDate = j.ExpiryDate.ToString("dd.MM.yyyy")
            });

            return View(viewModel);
        }

        // GET: /employer/positions/create
        [Route("employer/positions/create")]
        public ActionResult Create()
        {
            var viewModel = new EmployerJobPositionViewModel
            {
                JobTitle = "",
                JobDescription = "",
                ExpiryDate = DateTime.Now.ToString("dd.MM.yyyy")
            };
            return View(viewModel);
        }

        // POST: /employer/positions/create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("employer/positions/create")]
        public ActionResult Create(EmployerJobPositionViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var employer = Session["User"] as EmployerAccount;
            if (employer == null)
                return RedirectToLoginPage;

            var employerJobPosition = new EmployerJobPosition
            {
                JobTitle = model.JobTitle,
                JobDescription = model.JobDescription,
                PostedDate = DateTime.Now,
                ExpiryDate = DateTime.ParseExact(model.ExpiryDate, "dd.MM.yyyy", CultureInfo.InvariantCulture),
                EmployerId = employer.Id
            };

            UnitOfWork.EmployerJobPositions.Add(employerJobPosition);
            UnitOfWork.Complete();

            return RedirectToAction("Index", "EmployerJobPosition");
        }

        // GET: /employer/positions/{id}/edit
        [Route("employer/positions/{id}/edit")]
        public ActionResult Edit(int id)
        {
            var employerJobPosition = UnitOfWork.EmployerJobPositions.Find(id);
            if (employerJobPosition == null)
                return HttpNotFound();

            if (!IsEmployerLoggedIn(employerJobPosition.EmployerId))
                return RedirectToLoginPage;

            var viewModel = new EmployerJobPositionViewModel
            {
                JobTitle = employerJobPosition.JobTitle,
                JobDescription = employerJobPosition.JobDescription,
                PostedDate = employerJobPosition.PostedDate.ToString("dd.MM.yyyy"),
                ExpiryDate = employerJobPosition.ExpiryDate.ToString("dd.MM.yyyy")
            };
            return View(viewModel);
        }

        // POST: /employer/positions/{id}/edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("employer/positions/{id}/edit")]
        public ActionResult Edit(int id, EmployerJobPositionViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var employerJobPosition = UnitOfWork.EmployerJobPositions.Find(id);
            if (employerJobPosition == null)
                return HttpNotFound();

            if (!IsEmployerLoggedIn(employerJobPosition.EmployerId))
                return RedirectToLoginPage;

            employerJobPosition.JobTitle = model.JobTitle;
            employerJobPosition.JobDescription = model.JobDescription;
            employerJobPosition.ExpiryDate = DateTime.ParseExact(model.ExpiryDate, "dd.MM.yyyy", CultureInfo.InvariantCulture);

            UnitOfWork.Complete();

            return RedirectToAction("Index", "EmployerJobPosition");
        }

        // POST: /employer/positions/{id}/delete
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("employer/positions/{id}/delete")]
        public ActionResult Delete(int id)
        {
            var employerJobPosition = UnitOfWork.EmployerJobPositions.Find(id);
            if (employerJobPosition == null)
                return HttpNotFound();

            if (!IsEmployerLoggedIn(employerJobPosition.EmployerId))
                return RedirectToLoginPage;

            UnitOfWork.EmployerJobPositions.Remove(employerJobPosition);
            UnitOfWork.Complete();

            return RedirectToAction("Index", "EmployerJobPosition");
        }
    }
}