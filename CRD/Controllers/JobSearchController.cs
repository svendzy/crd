﻿using CRD.Core;
using CRD.Core.Attributes;
using CRD.Core.Models;
using CRD.Core.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRD.Controllers
{
    [CustomAuthorize(Roles = "JobSeeker")]
    public class JobSearchController : BaseController
    {

        public JobSearchController(
            IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        [HttpGet]
        [Route("jobseeker/positions/search")]
        public ActionResult Search()
        {
            return Search(new JobSearchViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("jobseeker/positions/search")]
        public ActionResult Search(JobSearchViewModel model)
        {
            var jobSeeker = Session["User"] as JobSeekerAccount;
            if (jobSeeker == null)
                return RedirectToLoginPage;

            var jobPositionExcepts = UnitOfWork.JobApplications.GetAppliedJobPositionsIds(jobSeeker.Id);

            var jobPositionsViewModel = new List<JobPositionViewModel>();

            var searchTerm = model.SearchTerm;
            var includeEmployers = model.IncludeEmployers;
            var includeRecruitmentAgencies = model.IncludeRecruitmentAgencies;
            var maxElementsToRetrieve = model.MaxElementsToRetrieve;

            if (includeEmployers)
            {
                var employerJobPositions = UnitOfWork.JobSearches
                    .SearchByEmployers(maxElementsToRetrieve, searchTerm)
                    .Where(j => !jobPositionExcepts.Contains(j.Id));

                var employerJobPositionViewModel = employerJobPositions
                    .Select(j => new EmployerJobPositionViewModel()
                    {
                        Id = j.Id,
                        CompanyName = j.Employer.CompanyName,
                        JobTitle = j.JobTitle,
                        JobDescription = j.JobDescription,
                        PostedDate = j.PostedDate.ToString("dd.MM.yyyy"),
                        ExpiryDate = j.ExpiryDate.ToString("dd.MM.yyyy")
                    });

                jobPositionsViewModel.AddRange(employerJobPositionViewModel);
            }

            if (jobPositionsViewModel.Count < maxElementsToRetrieve)
            {
                if (includeRecruitmentAgencies)
                {
                    var nElementsLeftToRetrieve = maxElementsToRetrieve - jobPositionsViewModel.Count;

                    var recruitmentAgencyJobPositions = UnitOfWork.JobSearches
                        .SearchByRecruiters(nElementsLeftToRetrieve, searchTerm)
                        .Where(j => !jobPositionExcepts.Contains(j.Id));

                    var recruitmentAgencyJobPositionViewModel = recruitmentAgencyJobPositions
                        .Select(j => new RecruitmentAgencyJobPositionViewModel()
                        {
                            Id = j.Id,
                            CompanyName = j.RecruitmentAgency.CompanyName,
                            JobTitle = j.JobTitle,
                            JobDescription = j.JobDescription,
                            PostedDate = j.PostedDate.ToString("dd.MM.yyyy"),
                            ExpiryDate = j.ExpiryDate.ToString("dd.MM.yyyy")
                        });
                    jobPositionsViewModel.AddRange(recruitmentAgencyJobPositionViewModel);
                }
            }

            var viewModel = model;
            viewModel.JobPositions = jobPositionsViewModel;

            return View("Listing", viewModel);
        }
    }
}