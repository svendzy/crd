﻿using CRD.Core;
using CRD.Core.Attributes;
using CRD.Core.Models;
using CRD.Core.ViewModels;
using System.Linq;
using System.Web.Mvc;

namespace CRD.Controllers
{
    [CustomAuthorize(Roles = "Employer")]
    public class EmployerContactPersonController : BaseController
    {

        public EmployerContactPersonController(
            IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        // GET: /employer/contacts
        [HttpGet]
        [Route("employer/contacts")]
        public ActionResult Index()
        {
            var employer = Session["User"] as EmployerAccount;
            if (employer == null)
                return RedirectToLoginPage;

            var contactPersons = UnitOfWork.EmployerContactPersons.GetAll(employer.Id);

            var viewModel = contactPersons.Select(j => new EmployerContactPersonViewModel()
            {
                Id = j.Id,
                Name = j.Name,
                Title = j.Title,
                Email = j.Email,
                Phone = j.Phone
            });

            return View(viewModel);
        }

        // GET: /employer/contacts/create
        [HttpGet]
        [Route("employer/contacts/create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: /employer/contacts/create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("employer/contacts/create")]
        public ActionResult Create(EmployerContactPersonViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var employer = Session["User"] as EmployerAccount;
            if (employer == null)
                return RedirectToLoginPage;

            var contactPerson = new EmployerContactPerson
            {
                Id = model.Id,
                Name = model.Name,
                Title = model.Title,
                Email = model.Email,
                Phone = model.Phone,
                EmployerId = employer.Id
            };

            UnitOfWork.EmployerContactPersons.Add(contactPerson);
            UnitOfWork.Complete();

            return RedirectToAction("Index", "EmployerContactPerson");
        }

        // GET: /employer/contacts/{id}/edit
        [HttpGet]
        [Route("employer/contacts/{id}/edit")]
        public ActionResult Edit(int id)
        {
            var contactPerson = UnitOfWork.EmployerContactPersons.Find(id);
            if (contactPerson == null)
                return HttpNotFound();

            if (!IsEmployerLoggedIn(contactPerson.EmployerId))
                return RedirectToLoginPage;

            var viewModel = new EmployerContactPersonViewModel()
            {
                Id = contactPerson.Id,
                Name = contactPerson.Name,
                Title = contactPerson.Title,
                Email = contactPerson.Email,
                Phone = contactPerson.Phone
            };
            return View(viewModel);
        }

        // POST: /employer/contacts/{id}/edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("employer/contacts/{id}/edit")]
        public ActionResult Edit(int id, EmployerContactPersonViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var contactPerson = UnitOfWork.EmployerContactPersons.Find(id);
            if (contactPerson == null)
                return HttpNotFound();

            if (!IsEmployerLoggedIn(contactPerson.EmployerId))
                return RedirectToLoginPage;

            contactPerson.Name = model.Name;
            contactPerson.Title = model.Title;
            contactPerson.Email = model.Email;
            contactPerson.Phone = model.Phone;

            UnitOfWork.Complete();

            return RedirectToAction("Index", "EmployerContactPerson");
        }

        // POST: /employer/contacts/{id}/edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("employer/contacts/{id}/delete")]
        public ActionResult Delete(int id)
        {
            var contactPerson = UnitOfWork.EmployerContactPersons.Find(id);
            if (contactPerson == null)
                return HttpNotFound();

            if (!IsEmployerLoggedIn(contactPerson.EmployerId))
                return RedirectToLoginPage;

            UnitOfWork.EmployerContactPersons.Remove(contactPerson);
            UnitOfWork.Complete();

            return RedirectToAction("Index", "EmployerContactPerson");
        }
    }
}