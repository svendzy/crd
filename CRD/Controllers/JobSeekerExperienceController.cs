﻿using CRD.Core;
using CRD.Core.Attributes;
using CRD.Core.Models;
using CRD.Core.ViewModels;
using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace CRD.Controllers
{
    [CustomAuthorize(Roles = "JobSeeker")]
    public class JobSeekerExperienceController : BaseController
    {
        public JobSeekerExperienceController(
            IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        // GET: /jobseeker/experiences
        [Route("jobseeker/experiences")]
        public ActionResult Index()
        {
            var jobSeeker = Session["User"] as JobSeekerAccount;

            var courses = UnitOfWork.JobSeekerExperiences.GetAll(jobSeeker.Id);

            var viewModel = courses.Select(c => new JobSeekerExperienceViewModel()
            {
                Id = c.Id,
                FromDate = c.FromDate.ToString("dd.MM.yyyy"),
                ToDate = c.ToDate.ToString("dd.MM.yyyy"),
                Place = c.Place,
                Description = c.Description
            });

            return View(viewModel);
        }

        // GET: /jobseeker/experiences/create
        [Route("jobseeker/experiences/create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: /jobseeker/experiences/create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("jobseeker/experiences/create")]
        public ActionResult Create(JobSeekerExperienceViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var jobSeeker = Session["User"] as JobSeekerAccount;
            if (jobSeeker == null)
                return RedirectToLoginPage;

            var jobSeekerExperience = new JobSeekerExperience
            {
                FromDate = DateTime.ParseExact(model.FromDate, "dd.MM.yyyy", CultureInfo.InvariantCulture),
                ToDate = DateTime.ParseExact(model.ToDate, "dd.MM.yyyy", CultureInfo.InvariantCulture),
                Place = model.Place,
                Description = model.Description,
                JobSeekerId = jobSeeker.Id
            };

            UnitOfWork.JobSeekerExperiences.Add(jobSeekerExperience);
            UnitOfWork.Complete();

            return RedirectToAction("Index", "JobSeekerExperience");
        }

        // GET: /jobseeker/experiences/{id}/edit
        [Route("jobseeker/experiences/{id}/edit")]
        public ActionResult Edit(int id)
        {
            var jobSeekerExperience = UnitOfWork.JobSeekerExperiences.Find(id);
            if (jobSeekerExperience == null)
                return HttpNotFound();

            if (!IsJobSeekerLoggedIn(jobSeekerExperience.JobSeekerId))
                return RedirectToLoginPage;

            var viewModel = new JobSeekerExperienceViewModel
            {
                FromDate = jobSeekerExperience.FromDate.ToString("dd.MM.yyyy"),
                ToDate = jobSeekerExperience.ToDate.ToString("dd.MM.yyyy"),
                Place = jobSeekerExperience.Place,
                Description = jobSeekerExperience.Description
            };
            return View(viewModel);
        }


        // POST: /jobseeker/experiences/{id}/edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("jobseeker/experiences/{id}/edit")]
        public ActionResult Edit(int id, JobSeekerExperienceViewModel model)
        {
            var jobSeekerExperience = UnitOfWork.JobSeekerExperiences.Find(id);
            if (jobSeekerExperience == null)
                return HttpNotFound();

            if (!IsJobSeekerLoggedIn(jobSeekerExperience.JobSeekerId))
                return RedirectToLoginPage;

            jobSeekerExperience.FromDate = DateTime.ParseExact(model.FromDate, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            jobSeekerExperience.ToDate = DateTime.ParseExact(model.ToDate, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            jobSeekerExperience.Place = model.Place;
            jobSeekerExperience.Description = model.Description;

            UnitOfWork.Complete();

            return RedirectToAction("Index", "JobSeekerExperience");
        }

        // POST: /jobseeker/experiences/{id}/delete
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("jobseeker/experiences/{id}/delete")]
        public ActionResult Delete(int id)
        {
            var jobSeekerExperience = UnitOfWork.JobSeekerExperiences.Find(id);
            if (jobSeekerExperience == null)
                return HttpNotFound();

            if (!IsJobSeekerLoggedIn(jobSeekerExperience.JobSeekerId))
                return RedirectToLoginPage;

            UnitOfWork.JobSeekerExperiences.Remove(jobSeekerExperience);
            UnitOfWork.Complete();;

            return RedirectToAction("Index", "JobSeekerExperience");
        }
    }
}