﻿using System.Web.Mvc;

namespace CRD.Controllers
{
    public class ErrorController : Controller
    {
        // GET: /Error
        [HttpGet]
        [Route("error")]
        public ActionResult Index()
        {
            return View();
        }
    }
}